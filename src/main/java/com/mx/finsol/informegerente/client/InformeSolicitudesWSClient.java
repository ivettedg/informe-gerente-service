package com.mx.finsol.informegerente.client;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.finsol.informegerente.exception.CustomException;
import com.mx.finsol.informegerente.model.dto.ErrorDetails;
import com.mx.finsol.informegerente.model.dto.SolicitudesBusquedaDTO;
import com.mx.finsol.informegerente.model.dto.SolicitudesBusquedaIntegrantesDTO;
import com.mx.finsol.informegerente.model.entity.solicitudes.SolicitudIntegrantes;
import com.mx.finsol.informegerente.model.entity.solicitudes.SolicitudesFinsol;
import com.mx.finsol.informegerente.util.StaticsConstants;

@Configuration
public class InformeSolicitudesWSClient {
	
	private static final RestTemplate rt = new RestTemplate();
	private static final ResourceBundle bundle = ResourceBundle.getBundle("application-dev");

	static ObjectMapper mapper = new ObjectMapper();
	/**
	 * Cliente WS para busqueda de solicitudes
	 * 
	 * @param solicitudesBusqueda
	 * @return List<SolicitudesFinsol>
	 */
	
	public static List<SolicitudesFinsol> busquedaSolicitudes(SolicitudesBusquedaDTO solicitudesBusqueda){
		
		List<SolicitudesFinsol> listSolicitudes = new ArrayList<>();
		ErrorDetails errorDetails = new ErrorDetails();
		
		try {
			String uriSolicitudes = bundle.getString(StaticsConstants.WS_URL_HOST_CLOUD_SOLICITUDES)
					+ bundle.getString(StaticsConstants.WS_DATOS_SOLICITUDES);
			HttpEntity<SolicitudesBusquedaDTO> requestEntity = new HttpEntity<SolicitudesBusquedaDTO>(solicitudesBusqueda);
			ResponseEntity<?> solicitudesTemp = rt.exchange(uriSolicitudes, HttpMethod.POST, requestEntity,
					new ParameterizedTypeReference<Object>() {
					});

			if (solicitudesTemp.getStatusCodeValue() == 200) {
				listSolicitudes = mapper.convertValue(solicitudesTemp.getBody(), new TypeReference<List<SolicitudesFinsol>>() {
				});
			} else if (solicitudesTemp.getStatusCodeValue() == 202) {
				errorDetails = mapper.convertValue(solicitudesTemp.getBody(), new TypeReference<ErrorDetails>() {
				});
				throw new CustomException(errorDetails.getDetails(), HttpStatus.ACCEPTED, errorDetails.getLevel());
			}

			return listSolicitudes;
		} catch (CustomException e) {
			throw e;
		} catch (RestClientException e) {
			throw new CustomException("No se pudieron consultar las solicitudes: " + e.getMessage(),
					HttpStatus.ACCEPTED, "error");
		}
			
	}
	
	/**
	 * Cliente WS para la busqueda de integrantes por solicitud
	 * 
	 * @param solicitudesIntegrantes
	 * @return List<SolicitudesFinsol>
	 */
    public static List<SolicitudIntegrantes> busquedaSolicitudesIntegrantes(SolicitudesBusquedaIntegrantesDTO solicitudesIntegrantes){
		
		List<SolicitudIntegrantes> listSolicitudesInt = new ArrayList<>();
		ErrorDetails errorDetails = new ErrorDetails();
		
		try {
			String uriSolicitudesInt = bundle.getString(StaticsConstants.WS_URL_HOST_CLOUD_SOLICITUDES)
					+ bundle.getString(StaticsConstants.WS_DATOS_SOLICITUDES_INTEGRANTES);
			HttpEntity<SolicitudesBusquedaIntegrantesDTO> requestEntity = new HttpEntity<SolicitudesBusquedaIntegrantesDTO>(solicitudesIntegrantes);
			ResponseEntity<?> solicitudesIntTemp = rt.exchange(uriSolicitudesInt, HttpMethod.POST, requestEntity,
					new ParameterizedTypeReference<Object>() {
					});

			if (solicitudesIntTemp.getStatusCodeValue() == 200) {
				listSolicitudesInt = mapper.convertValue(solicitudesIntTemp.getBody(), new TypeReference<List<SolicitudIntegrantes>>() {
				});
			} else if (solicitudesIntTemp.getStatusCodeValue() == 202) {
				errorDetails = mapper.convertValue(solicitudesIntTemp.getBody(), new TypeReference<ErrorDetails>() {
				});
				throw new CustomException(errorDetails.getDetails(), HttpStatus.ACCEPTED, errorDetails.getLevel());
			}

			return listSolicitudesInt;
		} catch (CustomException e) {
			throw e;
		} catch (RestClientException e) {
			throw new CustomException("No se pudo consultar los integrantes de la solicitud: " + e.getMessage(),
					HttpStatus.ACCEPTED, "error");
		}
			
	}

}
