package com.mx.finsol.informegerente.client;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.finsol.informegerente.exception.CustomException;
import com.mx.finsol.informegerente.model.dto.ContratosBusquedaDTO;
import com.mx.finsol.informegerente.model.dto.ContratosBusquedaIntegranteDTO;
import com.mx.finsol.informegerente.model.dto.ErrorDetails;
import com.mx.finsol.informegerente.model.entity.contratos.Contratos;
import com.mx.finsol.informegerente.model.entity.contratos.IntegranteGrupo;
import com.mx.finsol.informegerente.util.StaticsConstants;

@Configuration
public class InformeContratosWSClient {
	
	private static final RestTemplate rt = new RestTemplate();
	private static final ResourceBundle bundle = ResourceBundle.getBundle("application-dev");

	static ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * Cliente WS para consulta la tabla de contratos por alguno de los campos del contrato
	 * 
	 * @param contratosBusqueda
	 * @return List<ContratosFinsol>
	 */
	
    public static List<Contratos> busquedaContratos(ContratosBusquedaDTO contratosBusqueda){
		
		List<Contratos> listContratos = new ArrayList<>();
		ErrorDetails errorDetails = new ErrorDetails();
		
		try {
			String uriContratos = bundle.getString(StaticsConstants.WS_URL_HOST_CLOUD_CONTRATOS)
					+ bundle.getString(StaticsConstants.WS_DATOS_CONTRATOS);
			HttpEntity<ContratosBusquedaDTO> requestEntity = new HttpEntity<ContratosBusquedaDTO>(contratosBusqueda);
			ResponseEntity<?> contratosTemp = rt.exchange(uriContratos, HttpMethod.POST, requestEntity,
					new ParameterizedTypeReference<Object>() {
					});

			if (contratosTemp.getStatusCodeValue() == 200) {
				listContratos = mapper.convertValue(contratosTemp.getBody(), new TypeReference<List<Contratos>>() {
				});
			} else if (contratosTemp.getStatusCodeValue() == 202) {
				errorDetails = mapper.convertValue(contratosTemp.getBody(), new TypeReference<ErrorDetails>() {
				});
				throw new CustomException(errorDetails.getDetails(), HttpStatus.ACCEPTED, errorDetails.getLevel());
			}

			return listContratos;
		} catch (CustomException e) {
			throw e;
		} catch (RestClientException e) {
			throw new CustomException("No se pudieron consultar los contratos: " + e.getMessage(),
					HttpStatus.ACCEPTED, "error");
		}
			
	}
    
    /**
     * Cliente WS para consultar los integrantes de un contrato por uno o mas de los campos
     * 
     * @param contratosIntegrantes
     * @return List<ContratosFinsol>
     */
   public static List<IntegranteGrupo> busquedaContratosIntegrantes(ContratosBusquedaIntegranteDTO contratosIntegrantes){
		
		List<IntegranteGrupo> listContratosInt = new ArrayList<>();
		ErrorDetails errorDetails = new ErrorDetails();
		
		try {
			String uriContratosInt = bundle.getString(StaticsConstants.WS_URL_HOST_CLOUD_CONTRATOS)
					+ bundle.getString(StaticsConstants.WS_DATOS_CONTRATOS_INTEGRANTES);
			HttpEntity<ContratosBusquedaIntegranteDTO> requestEntity = new HttpEntity<ContratosBusquedaIntegranteDTO>(contratosIntegrantes);
			ResponseEntity<?> contratosIntTemp = rt.exchange(uriContratosInt, HttpMethod.POST, requestEntity,
					new ParameterizedTypeReference<Object>() {
					});

			if (contratosIntTemp.getStatusCodeValue() == 200) {
				listContratosInt = mapper.convertValue(contratosIntTemp.getBody(), new TypeReference<List<IntegranteGrupo>>() {
				});
			} else if (contratosIntTemp.getStatusCodeValue() == 202) {
				errorDetails = mapper.convertValue(contratosIntTemp.getBody(), new TypeReference<ErrorDetails>() {
				});
				throw new CustomException(errorDetails.getDetails(), HttpStatus.ACCEPTED, errorDetails.getLevel());
			}

			return listContratosInt;
		} catch (CustomException e) {
			throw e;
		} catch (RestClientException e) {
			throw new CustomException("No se pudo consultar los integrantes del contrato: " + e.getMessage(),
					HttpStatus.ACCEPTED, "error");
		}
			
	}

}
