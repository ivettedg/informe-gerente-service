package com.mx.finsol.informegerente.client;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.finsol.informegerente.exception.CustomException;
import com.mx.finsol.informegerente.model.dto.BuroBusquedaDTO;
import com.mx.finsol.informegerente.model.dto.ErrorDetails;
import com.mx.finsol.informegerente.model.entity.burocredito.OUTPersonaBuroBean;
import com.mx.finsol.informegerente.util.StaticsConstants;

@Configuration
public class InformeBuroWSClient {
	
	private static final RestTemplate rt = new RestTemplate();
	private static final ResourceBundle bundle = ResourceBundle.getBundle("application-dev");

	static ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * Cliente WS para consultar la información de buró
	 * 
	 * @param buroBusqueda
	 * @return
	 */
    public static List<OUTPersonaBuroBean> busquedaBuro(BuroBusquedaDTO buroBusqueda){
		
		List<OUTPersonaBuroBean> listBuro = new ArrayList<>();
		ErrorDetails errorDetails = new ErrorDetails();
		
		try {
			String uriBuro = bundle.getString(StaticsConstants.WS_URL_HOST_CLOUD_BURO)
					+ bundle.getString(StaticsConstants.WS_DATOS_BURO);
			HttpEntity<BuroBusquedaDTO> requestEntity = new HttpEntity<BuroBusquedaDTO>(buroBusqueda);
			ResponseEntity<?> buroTemp = rt.exchange(uriBuro, HttpMethod.POST, requestEntity,
					new ParameterizedTypeReference<Object>() {
					});

			if (buroTemp.getStatusCodeValue() == 200) {
				listBuro = mapper.convertValue(buroTemp.getBody(), new TypeReference<List<OUTPersonaBuroBean>>() {
				});
			} else if (buroTemp.getStatusCodeValue() == 202) {
				errorDetails = mapper.convertValue(buroTemp.getBody(), new TypeReference<ErrorDetails>() {
				});
				throw new CustomException(errorDetails.getDetails(), HttpStatus.ACCEPTED, errorDetails.getLevel());
			}

			return listBuro;
		} catch (CustomException e) {
			throw e;
		} catch (RestClientException e) {
			throw new CustomException("No se pudo consultar la informacion de buró: " + e.getMessage(),
					HttpStatus.ACCEPTED, "error");
		}
			
	}

}
