package com.mx.finsol.informegerente.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;

@ResponseStatus(HttpStatus.NOT_FOUND)
@Getter
public class CustomException  extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;
	private HttpStatus status;
	private String level;

	/**
	 * @param exception
	 * Constructor de exception para envÃ­o de string
	 * mensaje a travez de capas
	 */
	public CustomException(String message, HttpStatus status) {
	    this.message = message;
	    this.status = status;
	    this.level = "warning";
	 }
	
	public CustomException(String message, HttpStatus status, String level) {
		this.message = message;
		this.status = status;
		this.level = level;

	}
	
	
}
