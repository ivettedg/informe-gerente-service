package com.mx.finsol.informegerente.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.mx.finsol.informegerente.model.dto.ErrorDetails;
import com.mx.finsol.informegerente.response.ResponseError;
import com.mx.finsol.informegerente.util.UuidGenerator;

@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{
	
	@Autowired
	private UuidGenerator uuidGen;


	@ExceptionHandler(Exception.class)
	public final ResponseEntity<?> handleCodeExceptions(Exception e, ResponseError rs, HttpStatus status,
			ErrorDetails ed) {
		String uuid = uuidGen.generateUuid();
		// Valida si se recibe un responseStatus
		if (rs == null) {
			rs = new ResponseError();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			errors.toString();
			String[] codigoDesc = e.getMessage().toString().split(",");
			ErrorDetails detail = new ErrorDetails("CHRONOS500", codigoDesc[0], "ERROR", codigoDesc[1], new Date());
			rs.setErrorDetails(detail);
			rs.setRqUID(uuid);
			return new ResponseEntity<ResponseError>(rs, status);
		} else {
			rs.setErrorDetails(ed);
			rs.setRqUID(uuid);
			return new ResponseEntity<ResponseError>(rs, status);
		}
	}

}
