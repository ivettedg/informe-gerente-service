package com.mx.finsol.informegerente.util;

import java.util.UUID;

import org.springframework.stereotype.Component;

@Component
public class UuidGeneratorImpl implements UuidGenerator{

	@Override
	public String generateUuid() {
		return UUID.randomUUID().toString();
	}
}
