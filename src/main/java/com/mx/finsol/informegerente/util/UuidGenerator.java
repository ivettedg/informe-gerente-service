package com.mx.finsol.informegerente.util;

public interface UuidGenerator {
	
	String generateUuid();

}
