package com.mx.finsol.informegerente.util;

public class StaticsConstants {

	//ENDPOINT URL
	public static final String WS_URL_HOST_CLOUD_SOLICITUDES = "https://solicitudes-finsol-dot-findep-desarrollo-170215.appspot.com";
	public static final String WS_URL_HOST_CLOUD_CONTRATOS = "https://contratos-finsol-dot-findep-desarrollo-170215.uc.r.appspot.com";
	public static final String WS_URL_HOST_CLOUD_BURO = "https://buro-credito-service-dot-findep-desarrollo-170215.appspot.com";
	
	
	//WS SOLICITUDES
	public static final String WS_DATOS_SOLICITUDES = "ws.path.datosSolicitudes";
	public static final String WS_DATOS_SOLICITUDES_INTEGRANTES = "ws.path.datosSolicitudesIntegrantes";
	
	//WS CONTRATOS
	public static final String WS_DATOS_CONTRATOS = "ws.path.datosContratos";
	public static final String WS_DATOS_CONTRATOS_INTEGRANTES = "ws.path.datosContratosIntegrantes";
	
	//WS BURO
	public static final String WS_DATOS_BURO = "ws.path.datosBuro";
}
