package com.mx.finsol.informegerente.response;

import java.io.Serializable;

import com.mx.finsol.informegerente.model.dto.ErrorDetails;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseError implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ErrorDetails errorDetails;
	private String rqUID;

}
