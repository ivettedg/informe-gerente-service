package com.mx.finsol.informegerente.model.entity.burocredito;

public class OUTDireccionBean {
	
	private String etiquetaEncabezadoSegmento;
	private String primeraLineaDireccion;
	private String segundaLineaDireccion;
	private String coloniaPoblacion;
	private String delegacionMunicipio;
	private String ciudad;
	private String estado;
	private String codigoPostal;
	private String fechaResidencia;
	private String numeroTelefono;
	private String extensionTelefonica;
	private String numeroFax;
	private String tipoDomicilio;
	private String indicadorEspecialDomicilio;
	private String fechaReporteDireccion;
	private String origenDomicilio; // Nueva etiqueta Enero 2020

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getPrimeraLineaDireccion() {
		return primeraLineaDireccion;
	}

	public void setPrimeraLineaDireccion(String primeraLineaDireccion) {
		this.primeraLineaDireccion = primeraLineaDireccion;
	}

	public String getSegundaLineaDireccion() {
		return segundaLineaDireccion;
	}

	public void setSegundaLineaDireccion(String segundaLineaDireccion) {
		this.segundaLineaDireccion = segundaLineaDireccion;
	}

	public String getColoniaPoblacion() {
		return coloniaPoblacion;
	}

	public void setColoniaPoblacion(String coloniaPoblacion) {
		this.coloniaPoblacion = coloniaPoblacion;
	}

	public String getDelegacionMunicipio() {
		return delegacionMunicipio;
	}

	public void setDelegacionMunicipio(String delegacionMunicipio) {
		this.delegacionMunicipio = delegacionMunicipio;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getFechaResidencia() {
		return fechaResidencia;
	}

	public void setFechaResidencia(String fechaResidencia) {
		this.fechaResidencia = fechaResidencia;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getExtensionTelefonica() {
		return extensionTelefonica;
	}

	public void setExtensionTelefonica(String extensionTelefonica) {
		this.extensionTelefonica = extensionTelefonica;
	}

	public String getNumeroFax() {
		return numeroFax;
	}

	public void setNumeroFax(String numeroFax) {
		this.numeroFax = numeroFax;
	}

	public String getTipoDomicilio() {
		return tipoDomicilio;
	}

	public void setTipoDomicilio(String tipoDomicilio) {
		this.tipoDomicilio = tipoDomicilio;
	}

	public String getIndicadorEspecialDomicilio() {
		return indicadorEspecialDomicilio;
	}

	public void setIndicadorEspecialDomicilio(String indicadorEspecialDomicilio) {
		this.indicadorEspecialDomicilio = indicadorEspecialDomicilio;
	}

	public String getFechaReporteDireccion() {
		return fechaReporteDireccion;
	}

	public void setFechaReporteDireccion(String fechaReporteDireccion) {
		this.fechaReporteDireccion = fechaReporteDireccion;
	}

	public String getOrigenDomicilio() {
		return origenDomicilio;
	}

	public void setOrigenDomicilio(String origenDomicilio) {
		this.origenDomicilio = origenDomicilio;
	}

	@Override
	public String toString() {
		return "OUTDireccionBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", primeraLineaDireccion="
				+ primeraLineaDireccion + ", segundaLineaDireccion=" + segundaLineaDireccion + ", coloniaPoblacion="
				+ coloniaPoblacion + ", delegacionMunicipio=" + delegacionMunicipio + ", ciudad=" + ciudad + ", estado="
				+ estado + ", codigoPostal=" + codigoPostal + ", fechaResidencia=" + fechaResidencia
				+ ", numeroTelefono=" + numeroTelefono + ", extensionTelefonica=" + extensionTelefonica + ", numeroFax="
				+ numeroFax + ", tipoDomicilio=" + tipoDomicilio + ", indicadorEspecialDomicilio="
				+ indicadorEspecialDomicilio + ", fechaReporteDireccion=" + fechaReporteDireccion + ", origenDomicilio="
				+ origenDomicilio + "]";
	}

}
