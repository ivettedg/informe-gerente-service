package com.mx.finsol.informegerente.model.entity.burocredito;


public class ElementoSegmentoCadenaBean {

	private String etiquetaNombreSegmento;// Nombre del segmento o posición de lcampo en la cadena
	private String valorCampo; // El valor del campo
	private String tipo; // N:Numerico, A:Alfabetico, AN:Alfanumerico, F:Fecha,M:Moneda, ESP: Especial
	private String longitud; // Tamaño del segmento
	private Boolean longitudFija; // true para longitud fija, false para longitud variable
	private String validacion; // R:Requerido, O:Opcional, C:Condicionado
	private Integer posicionInicial; // numero de caracter donde inicia el segmento
	private Integer posicionFinal; // numero de caracter donde termina el segmento
	
//	public SegmentoCadenaBean(String etiquetaNombreSegmento, String longitud, String valorCampo) {
//	super();
//	this.etiquetaNombreSegmento = etiquetaNombreSegmento;
//	this.longitud = longitud;
//	this.valorCampo = valorCampo;
//}

public void setDefaultSegmento(String etiquetaNombreSegmento, String longitud, String valorCampo) {
	this.etiquetaNombreSegmento = etiquetaNombreSegmento;
	this.longitud = longitud;
	this.valorCampo = valorCampo;
}

public String getEtiquetaNombreSegmento() {
	return etiquetaNombreSegmento;
}

public void setEtiquetaNombreSegmento(String etiquetaNombreSegmento) {
	this.etiquetaNombreSegmento = etiquetaNombreSegmento;
}

public String getValorCampo() {
	return valorCampo;
}

public void setValorCampo(String valorCampo) {
	this.valorCampo = valorCampo;
}

public String getTipo() {
	return tipo;
}

public void setTipo(String tipo) {
	this.tipo = tipo;
}

public String getLongitud() {
	return longitud;
}

public void setLongitud(String longitud) {
	this.longitud = longitud;
}

public Boolean getLongitudFija() {
	return longitudFija;
}

public void setLongitudFija(Boolean longitudFija) {
	this.longitudFija = longitudFija;
}

public String getValidacion() {
	return validacion;
}

public void setValidacion(String validacion) {
	this.validacion = validacion;
}

public Integer getPosicionInicial() {
	return posicionInicial;
}

public void setPosicionInicial(Integer posicionInicial) {
	this.posicionInicial = posicionInicial;
}

public Integer getPosicionFinal() {
	return posicionFinal;
}

public void setPosicionFinal(Integer posicionFinal) {
	this.posicionFinal = posicionFinal;
}

@Override
public String toString() {
	return "SegmentoCadenaBean [etiquetaNombreSegmento=" + etiquetaNombreSegmento + ", valorCampo=" + valorCampo
			+ ", tipo=" + tipo + ", longitud=" + longitud + ", longitudFija=" + longitudFija + ", validacion="
			+ validacion + ", posicionInicial=" + posicionInicial + ", posicionFinal=" + posicionFinal + "]";
}

}
