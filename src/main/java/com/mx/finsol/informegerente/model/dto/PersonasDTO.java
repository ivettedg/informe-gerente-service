package com.mx.finsol.informegerente.model.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonasDTO {
	
	private EncabezadoDTO encabezado;
	private NombreDTO nombre;
	private List<DomiciliosDTO> domicilios;
	private AutenticaDTO autentica;
	

}
