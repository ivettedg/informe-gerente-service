package com.mx.finsol.informegerente.model.service;

import java.util.List;

import com.mx.finsol.informegerente.model.dto.BuroBusquedaDTO;
import com.mx.finsol.informegerente.model.entity.burocredito.OUTPersonaBuroBean;

public interface InformeGerenteService {

	
	public List<OUTPersonaBuroBean> consultarBuroClientes(BuroBusquedaDTO buroBusquedaDTO);
	
	public List<> consultarBuroClientes(BuroBusquedaDTO buroBusquedaDTO);
	
}
