package com.mx.finsol.informegerente.model.entity.burocredito;

public class OUTCierreBean {

	
	private String etiquetaEncabezadoSegmento;
	private String longitudTransmision;
	private String numeroControlConsulta;
	private String finRegistroRespuesta;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getLongitudTransmision() {
		return longitudTransmision;
	}

	public void setLongitudTransmision(String longitudTransmision) {
		this.longitudTransmision = longitudTransmision;
	}

	public String getNumeroControlConsulta() {
		return numeroControlConsulta;
	}

	public void setNumeroControlConsulta(String numeroControlConsulta) {
		this.numeroControlConsulta = numeroControlConsulta;
	}

	public String getFinRegistroRespuesta() {
		return finRegistroRespuesta;
	}

	public void setFinRegistroRespuesta(String finRegistroRespuesta) {
		this.finRegistroRespuesta = finRegistroRespuesta;
	}

	@Override
	public String toString() {
		return "OUTCierreBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", longitudTransmision="
				+ longitudTransmision + ", numeroControlConsulta=" + numeroControlConsulta + ", finRegistroRespuesta="
				+ finRegistroRespuesta + "]";
	}
}
