package com.mx.finsol.informegerente.model.entity.burocredito;

public class RequestResponseBuroBean {

	
	private String requestId;
	private String inputBuro;
	private String outputBuro;

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getInputBuro() {
		return inputBuro;
	}

	public void setInputBuro(String inputBuro) {
		this.inputBuro = inputBuro;
	}

	public String getOutputBuro() {
		return outputBuro;
	}

	public void setOutputBuro(String outputBuro) {
		this.outputBuro = outputBuro;
	}

	@Override
	public String toString() {
		return "RequestResponseBuroBean [requestId=" + requestId + ", inputBuro=" + inputBuro + ", outputBuro="
				+ outputBuro + "]";
	}
}
