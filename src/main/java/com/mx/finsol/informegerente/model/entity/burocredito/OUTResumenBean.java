package com.mx.finsol.informegerente.model.entity.burocredito;

public class OUTResumenBean {

	
	private String etiquetaEncabezadoSegmento;
	private String fechaIntegracionBaseDatos;
	private String numeroCuentasMop7;
	private String numeroCuentasMop6;
	private String numeroCuentasMop5;
	private String numeroCuentasMop4;
	private String numeroCuentasMop3;
	private String numeroCuentasMop2;
	private String numeroCuentasMop1;
	private String numeroCuentasMop0;
	private String numeroCuentasMopUR;
	private String numeroCuentas;
	private String numeroCuentasPagosFijosHipotecario;
	private String numeroCuentasRevolventesSinLimitePrestablecido;
	private String numeroCuentasCerradas;
	private String numeroCuentasMorosidadActual;
	private String numeroCuentasConHistorialMorosidad;
	private String numeroCuentasAclaracion;
	private String numeroSolicitudesConsulta;
	private String nuevaDireccionUltimos60Dias;
	private String mensajeAlerta;
	private String declarativa;
	private String moendaDelCredito;
	private String totalCredtosMaximosCuentaRevolventeSinLimitePrestablecido;
	private String totalLimitesCreditoCuentasRevolventesSinLimitePrestablecido;
	private String totalSaldosActualesCuentasRevolventesSinLimitePrestablecido;
	private String totalSaldosVencidosCuentasRevolventesSinLimitePrestablecido;
	private String totalImportePagoCuentasRevolventesSinLimitePrestablecido;
	private String porcentajeLimiteCreditoUtilizadoCuentasRevolventesSinLimitePrestablecido;
	private String totalCreditosMaximosCuentasPagosFijosHipotecarios;
	private String totalSaldosActualesCuentasPagosFijosHipotecarios;
	private String totalSaldosVencidosCuentasPagosFijosHipotecarios;
	private String totalImportePagoCuentasPagosFijosHipotecarios;
	private String numeroCuentasMop96;
	private String numeroCuentasMop97;
	private String numeroCuentasMop99;
	private String fechaAperturaCuentaMasAntigua;
	private String fechaAperturaCuentaMasReciente;
	private String numeroSolicitudesInformeBuro;
	private String fechaConsultaMasReciente;
	private String numeroCuentasDespachoCobranzaAdministradoraCartera;
	private String fechaAperturaMasRecienteCuentaDespachoCobranzaAdministradoraCartera;
	private String numeroSolicitudesInformeBuroREalizadasDespachosCobranza;
	private String fechaConsultaMasRecienteRealizadaDespachoobranzaAdministradoraCartera;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getFechaIntegracionBaseDatos() {
		return fechaIntegracionBaseDatos;
	}

	public void setFechaIntegracionBaseDatos(String fechaIntegracionBaseDatos) {
		this.fechaIntegracionBaseDatos = fechaIntegracionBaseDatos;
	}

	public String getNumeroCuentasMop7() {
		return numeroCuentasMop7;
	}

	public void setNumeroCuentasMop7(String numeroCuentasMop7) {
		this.numeroCuentasMop7 = numeroCuentasMop7;
	}

	public String getNumeroCuentasMop6() {
		return numeroCuentasMop6;
	}

	public void setNumeroCuentasMop6(String numeroCuentasMop6) {
		this.numeroCuentasMop6 = numeroCuentasMop6;
	}

	public String getNumeroCuentasMop5() {
		return numeroCuentasMop5;
	}

	public void setNumeroCuentasMop5(String numeroCuentasMop5) {
		this.numeroCuentasMop5 = numeroCuentasMop5;
	}

	public String getNumeroCuentasMop4() {
		return numeroCuentasMop4;
	}

	public void setNumeroCuentasMop4(String numeroCuentasMop4) {
		this.numeroCuentasMop4 = numeroCuentasMop4;
	}

	public String getNumeroCuentasMop3() {
		return numeroCuentasMop3;
	}

	public void setNumeroCuentasMop3(String numeroCuentasMop3) {
		this.numeroCuentasMop3 = numeroCuentasMop3;
	}

	public String getNumeroCuentasMop2() {
		return numeroCuentasMop2;
	}

	public void setNumeroCuentasMop2(String numeroCuentasMop2) {
		this.numeroCuentasMop2 = numeroCuentasMop2;
	}

	public String getNumeroCuentasMop1() {
		return numeroCuentasMop1;
	}

	public void setNumeroCuentasMop1(String numeroCuentasMop1) {
		this.numeroCuentasMop1 = numeroCuentasMop1;
	}

	public String getNumeroCuentasMop0() {
		return numeroCuentasMop0;
	}

	public void setNumeroCuentasMop0(String numeroCuentasMop0) {
		this.numeroCuentasMop0 = numeroCuentasMop0;
	}

	public String getNumeroCuentasMopUR() {
		return numeroCuentasMopUR;
	}

	public void setNumeroCuentasMopUR(String numeroCuentasMopUR) {
		this.numeroCuentasMopUR = numeroCuentasMopUR;
	}

	public String getNumeroCuentas() {
		return numeroCuentas;
	}

	public void setNumeroCuentas(String numeroCuentas) {
		this.numeroCuentas = numeroCuentas;
	}

	public String getNumeroCuentasPagosFijosHipotecario() {
		return numeroCuentasPagosFijosHipotecario;
	}

	public void setNumeroCuentasPagosFijosHipotecario(String numeroCuentasPagosFijosHipotecario) {
		this.numeroCuentasPagosFijosHipotecario = numeroCuentasPagosFijosHipotecario;
	}

	public String getNumeroCuentasRevolventesSinLimitePrestablecido() {
		return numeroCuentasRevolventesSinLimitePrestablecido;
	}

	public void setNumeroCuentasRevolventesSinLimitePrestablecido(
			String numeroCuentasRevolventesSinLimitePrestablecido) {
		this.numeroCuentasRevolventesSinLimitePrestablecido = numeroCuentasRevolventesSinLimitePrestablecido;
	}

	public String getNumeroCuentasCerradas() {
		return numeroCuentasCerradas;
	}

	public void setNumeroCuentasCerradas(String numeroCuentasCerradas) {
		this.numeroCuentasCerradas = numeroCuentasCerradas;
	}

	public String getNumeroCuentasMorosidadActual() {
		return numeroCuentasMorosidadActual;
	}

	public void setNumeroCuentasMorosidadActual(String numeroCuentasMorosidadActual) {
		this.numeroCuentasMorosidadActual = numeroCuentasMorosidadActual;
	}

	public String getNumeroCuentasConHistorialMorosidad() {
		return numeroCuentasConHistorialMorosidad;
	}

	public void setNumeroCuentasConHistorialMorosidad(String numeroCuentasConHistorialMorosidad) {
		this.numeroCuentasConHistorialMorosidad = numeroCuentasConHistorialMorosidad;
	}

	public String getNumeroCuentasAclaracion() {
		return numeroCuentasAclaracion;
	}

	public void setNumeroCuentasAclaracion(String numeroCuentasAclaracion) {
		this.numeroCuentasAclaracion = numeroCuentasAclaracion;
	}

	public String getNumeroSolicitudesConsulta() {
		return numeroSolicitudesConsulta;
	}

	public void setNumeroSolicitudesConsulta(String numeroSolicitudesConsulta) {
		this.numeroSolicitudesConsulta = numeroSolicitudesConsulta;
	}

	public String getNuevaDireccionUltimos60Dias() {
		return nuevaDireccionUltimos60Dias;
	}

	public void setNuevaDireccionUltimos60Dias(String nuevaDireccionUltimos60Dias) {
		this.nuevaDireccionUltimos60Dias = nuevaDireccionUltimos60Dias;
	}

	public String getMensajeAlerta() {
		return mensajeAlerta;
	}

	public void setMensajeAlerta(String mensajeAlerta) {
		this.mensajeAlerta = mensajeAlerta;
	}

	public String getDeclarativa() {
		return declarativa;
	}

	public void setDeclarativa(String declarativa) {
		this.declarativa = declarativa;
	}

	public String getMoendaDelCredito() {
		return moendaDelCredito;
	}

	public void setMoendaDelCredito(String moendaDelCredito) {
		this.moendaDelCredito = moendaDelCredito;
	}

	public String getTotalCredtosMaximosCuentaRevolventeSinLimitePrestablecido() {
		return totalCredtosMaximosCuentaRevolventeSinLimitePrestablecido;
	}

	public void setTotalCredtosMaximosCuentaRevolventeSinLimitePrestablecido(
			String totalCredtosMaximosCuentaRevolventeSinLimitePrestablecido) {
		this.totalCredtosMaximosCuentaRevolventeSinLimitePrestablecido = totalCredtosMaximosCuentaRevolventeSinLimitePrestablecido;
	}

	public String getTotalLimitesCreditoCuentasRevolventesSinLimitePrestablecido() {
		return totalLimitesCreditoCuentasRevolventesSinLimitePrestablecido;
	}

	public void setTotalLimitesCreditoCuentasRevolventesSinLimitePrestablecido(
			String totalLimitesCreditoCuentasRevolventesSinLimitePrestablecido) {
		this.totalLimitesCreditoCuentasRevolventesSinLimitePrestablecido = totalLimitesCreditoCuentasRevolventesSinLimitePrestablecido;
	}

	public String getTotalSaldosActualesCuentasRevolventesSinLimitePrestablecido() {
		return totalSaldosActualesCuentasRevolventesSinLimitePrestablecido;
	}

	public void setTotalSaldosActualesCuentasRevolventesSinLimitePrestablecido(
			String totalSaldosActualesCuentasRevolventesSinLimitePrestablecido) {
		this.totalSaldosActualesCuentasRevolventesSinLimitePrestablecido = totalSaldosActualesCuentasRevolventesSinLimitePrestablecido;
	}

	public String getTotalSaldosVencidosCuentasRevolventesSinLimitePrestablecido() {
		return totalSaldosVencidosCuentasRevolventesSinLimitePrestablecido;
	}

	public void setTotalSaldosVencidosCuentasRevolventesSinLimitePrestablecido(
			String totalSaldosVencidosCuentasRevolventesSinLimitePrestablecido) {
		this.totalSaldosVencidosCuentasRevolventesSinLimitePrestablecido = totalSaldosVencidosCuentasRevolventesSinLimitePrestablecido;
	}

	public String getTotalImportePagoCuentasRevolventesSinLimitePrestablecido() {
		return totalImportePagoCuentasRevolventesSinLimitePrestablecido;
	}

	public void setTotalImportePagoCuentasRevolventesSinLimitePrestablecido(
			String totalImportePagoCuentasRevolventesSinLimitePrestablecido) {
		this.totalImportePagoCuentasRevolventesSinLimitePrestablecido = totalImportePagoCuentasRevolventesSinLimitePrestablecido;
	}

	public String getPorcentajeLimiteCreditoUtilizadoCuentasRevolventesSinLimitePrestablecido() {
		return porcentajeLimiteCreditoUtilizadoCuentasRevolventesSinLimitePrestablecido;
	}

	public void setPorcentajeLimiteCreditoUtilizadoCuentasRevolventesSinLimitePrestablecido(
			String porcentajeLimiteCreditoUtilizadoCuentasRevolventesSinLimitePrestablecido) {
		this.porcentajeLimiteCreditoUtilizadoCuentasRevolventesSinLimitePrestablecido = porcentajeLimiteCreditoUtilizadoCuentasRevolventesSinLimitePrestablecido;
	}

	public String getTotalCreditosMaximosCuentasPagosFijosHipotecarios() {
		return totalCreditosMaximosCuentasPagosFijosHipotecarios;
	}

	public void setTotalCreditosMaximosCuentasPagosFijosHipotecarios(
			String totalCreditosMaximosCuentasPagosFijosHipotecarios) {
		this.totalCreditosMaximosCuentasPagosFijosHipotecarios = totalCreditosMaximosCuentasPagosFijosHipotecarios;
	}

	public String getTotalSaldosActualesCuentasPagosFijosHipotecarios() {
		return totalSaldosActualesCuentasPagosFijosHipotecarios;
	}

	public void setTotalSaldosActualesCuentasPagosFijosHipotecarios(
			String totalSaldosActualesCuentasPagosFijosHipotecarios) {
		this.totalSaldosActualesCuentasPagosFijosHipotecarios = totalSaldosActualesCuentasPagosFijosHipotecarios;
	}

	public String getTotalSaldosVencidosCuentasPagosFijosHipotecarios() {
		return totalSaldosVencidosCuentasPagosFijosHipotecarios;
	}

	public void setTotalSaldosVencidosCuentasPagosFijosHipotecarios(
			String totalSaldosVencidosCuentasPagosFijosHipotecarios) {
		this.totalSaldosVencidosCuentasPagosFijosHipotecarios = totalSaldosVencidosCuentasPagosFijosHipotecarios;
	}

	public String getTotalImportePagoCuentasPagosFijosHipotecarios() {
		return totalImportePagoCuentasPagosFijosHipotecarios;
	}

	public void setTotalImportePagoCuentasPagosFijosHipotecarios(String totalImportePagoCuentasPagosFijosHipotecarios) {
		this.totalImportePagoCuentasPagosFijosHipotecarios = totalImportePagoCuentasPagosFijosHipotecarios;
	}

	public String getNumeroCuentasMop96() {
		return numeroCuentasMop96;
	}

	public void setNumeroCuentasMop96(String numeroCuentasMop96) {
		this.numeroCuentasMop96 = numeroCuentasMop96;
	}

	public String getNumeroCuentasMop97() {
		return numeroCuentasMop97;
	}

	public void setNumeroCuentasMop97(String numeroCuentasMop97) {
		this.numeroCuentasMop97 = numeroCuentasMop97;
	}

	public String getNumeroCuentasMop99() {
		return numeroCuentasMop99;
	}

	public void setNumeroCuentasMop99(String numeroCuentasMop99) {
		this.numeroCuentasMop99 = numeroCuentasMop99;
	}

	public String getFechaAperturaCuentaMasAntigua() {
		return fechaAperturaCuentaMasAntigua;
	}

	public void setFechaAperturaCuentaMasAntigua(String fechaAperturaCuentaMasAntigua) {
		this.fechaAperturaCuentaMasAntigua = fechaAperturaCuentaMasAntigua;
	}

	public String getFechaAperturaCuentaMasReciente() {
		return fechaAperturaCuentaMasReciente;
	}

	public void setFechaAperturaCuentaMasReciente(String fechaAperturaCuentaMasReciente) {
		this.fechaAperturaCuentaMasReciente = fechaAperturaCuentaMasReciente;
	}

	public String getNumeroSolicitudesInformeBuro() {
		return numeroSolicitudesInformeBuro;
	}

	public void setNumeroSolicitudesInformeBuro(String numeroSolicitudesInformeBuro) {
		this.numeroSolicitudesInformeBuro = numeroSolicitudesInformeBuro;
	}

	public String getFechaConsultaMasReciente() {
		return fechaConsultaMasReciente;
	}

	public void setFechaConsultaMasReciente(String fechaConsultaMasReciente) {
		this.fechaConsultaMasReciente = fechaConsultaMasReciente;
	}

	public String getNumeroCuentasDespachoCobranzaAdministradoraCartera() {
		return numeroCuentasDespachoCobranzaAdministradoraCartera;
	}

	public void setNumeroCuentasDespachoCobranzaAdministradoraCartera(
			String numeroCuentasDespachoCobranzaAdministradoraCartera) {
		this.numeroCuentasDespachoCobranzaAdministradoraCartera = numeroCuentasDespachoCobranzaAdministradoraCartera;
	}

	public String getFechaAperturaMasRecienteCuentaDespachoCobranzaAdministradoraCartera() {
		return fechaAperturaMasRecienteCuentaDespachoCobranzaAdministradoraCartera;
	}

	public void setFechaAperturaMasRecienteCuentaDespachoCobranzaAdministradoraCartera(
			String fechaAperturaMasRecienteCuentaDespachoCobranzaAdministradoraCartera) {
		this.fechaAperturaMasRecienteCuentaDespachoCobranzaAdministradoraCartera = fechaAperturaMasRecienteCuentaDespachoCobranzaAdministradoraCartera;
	}

	public String getNumeroSolicitudesInformeBuroREalizadasDespachosCobranza() {
		return numeroSolicitudesInformeBuroREalizadasDespachosCobranza;
	}

	public void setNumeroSolicitudesInformeBuroREalizadasDespachosCobranza(
			String numeroSolicitudesInformeBuroREalizadasDespachosCobranza) {
		this.numeroSolicitudesInformeBuroREalizadasDespachosCobranza = numeroSolicitudesInformeBuroREalizadasDespachosCobranza;
	}

	public String getFechaConsultaMasRecienteRealizadaDespachoobranzaAdministradoraCartera() {
		return fechaConsultaMasRecienteRealizadaDespachoobranzaAdministradoraCartera;
	}

	public void setFechaConsultaMasRecienteRealizadaDespachoobranzaAdministradoraCartera(
			String fechaConsultaMasRecienteRealizadaDespachoobranzaAdministradoraCartera) {
		this.fechaConsultaMasRecienteRealizadaDespachoobranzaAdministradoraCartera = fechaConsultaMasRecienteRealizadaDespachoobranzaAdministradoraCartera;
	}

	@Override
	public String toString() {
		return "OUTResumenBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento
				+ ", fechaIntegracionBaseDatos=" + fechaIntegracionBaseDatos + ", numeroCuentasMop7="
				+ numeroCuentasMop7 + ", numeroCuentasMop6=" + numeroCuentasMop6 + ", numeroCuentasMop5="
				+ numeroCuentasMop5 + ", numeroCuentasMop4=" + numeroCuentasMop4 + ", numeroCuentasMop3="
				+ numeroCuentasMop3 + ", numeroCuentasMop2=" + numeroCuentasMop2 + ", numeroCuentasMop1="
				+ numeroCuentasMop1 + ", numeroCuentasMop0=" + numeroCuentasMop0 + ", numeroCuentasMopUR="
				+ numeroCuentasMopUR + ", numeroCuentas=" + numeroCuentas + ", numeroCuentasPagosFijosHipotecario="
				+ numeroCuentasPagosFijosHipotecario + ", numeroCuentasRevolventesSinLimitePrestablecido="
				+ numeroCuentasRevolventesSinLimitePrestablecido + ", numeroCuentasCerradas=" + numeroCuentasCerradas
				+ ", numeroCuentasMorosidadActual=" + numeroCuentasMorosidadActual
				+ ", numeroCuentasConHistorialMorosidad=" + numeroCuentasConHistorialMorosidad
				+ ", numeroCuentasAclaracion=" + numeroCuentasAclaracion + ", numeroSolicitudesConsulta="
				+ numeroSolicitudesConsulta + ", nuevaDireccionUltimos60Dias=" + nuevaDireccionUltimos60Dias
				+ ", mensajeAlerta=" + mensajeAlerta + ", declarativa=" + declarativa + ", moendaDelCredito="
				+ moendaDelCredito + ", totalCredtosMaximosCuentaRevolventeSinLimitePrestablecido="
				+ totalCredtosMaximosCuentaRevolventeSinLimitePrestablecido
				+ ", totalLimitesCreditoCuentasRevolventesSinLimitePrestablecido="
				+ totalLimitesCreditoCuentasRevolventesSinLimitePrestablecido
				+ ", totalSaldosActualesCuentasRevolventesSinLimitePrestablecido="
				+ totalSaldosActualesCuentasRevolventesSinLimitePrestablecido
				+ ", totalSaldosVencidosCuentasRevolventesSinLimitePrestablecido="
				+ totalSaldosVencidosCuentasRevolventesSinLimitePrestablecido
				+ ", totalImportePagoCuentasRevolventesSinLimitePrestablecido="
				+ totalImportePagoCuentasRevolventesSinLimitePrestablecido
				+ ", porcentajeLimiteCreditoUtilizadoCuentasRevolventesSinLimitePrestablecido="
				+ porcentajeLimiteCreditoUtilizadoCuentasRevolventesSinLimitePrestablecido
				+ ", totalCreditosMaximosCuentasPagosFijosHipotecarios="
				+ totalCreditosMaximosCuentasPagosFijosHipotecarios
				+ ", totalSaldosActualesCuentasPagosFijosHipotecarios="
				+ totalSaldosActualesCuentasPagosFijosHipotecarios
				+ ", totalSaldosVencidosCuentasPagosFijosHipotecarios="
				+ totalSaldosVencidosCuentasPagosFijosHipotecarios + ", totalImportePagoCuentasPagosFijosHipotecarios="
				+ totalImportePagoCuentasPagosFijosHipotecarios + ", numeroCuentasMop96=" + numeroCuentasMop96
				+ ", numeroCuentasMop97=" + numeroCuentasMop97 + ", numeroCuentasMop99=" + numeroCuentasMop99
				+ ", fechaAperturaCuentaMasAntigua=" + fechaAperturaCuentaMasAntigua
				+ ", fechaAperturaCuentaMasReciente=" + fechaAperturaCuentaMasReciente
				+ ", numeroSolicitudesInformeBuro=" + numeroSolicitudesInformeBuro + ", fechaConsultaMasReciente="
				+ fechaConsultaMasReciente + ", numeroCuentasDespachoCobranzaAdministradoraCartera="
				+ numeroCuentasDespachoCobranzaAdministradoraCartera
				+ ", fechaAperturaMasRecienteCuentaDespachoCobranzaAdministradoraCartera="
				+ fechaAperturaMasRecienteCuentaDespachoCobranzaAdministradoraCartera
				+ ", numeroSolicitudesInformeBuroREalizadasDespachosCobranza="
				+ numeroSolicitudesInformeBuroREalizadasDespachosCobranza
				+ ", fechaConsultaMasRecienteRealizadaDespachoobranzaAdministradoraCartera="
				+ fechaConsultaMasRecienteRealizadaDespachoobranzaAdministradoraCartera + "]";
	}
}
