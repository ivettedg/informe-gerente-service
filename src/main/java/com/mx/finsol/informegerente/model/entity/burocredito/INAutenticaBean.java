package com.mx.finsol.informegerente.model.entity.burocredito;

public class INAutenticaBean {

	private String etiquetaEncabezadoSegmento;
	private String tipoReporte;
	private String tipoSalida;
	private String referenciaOperador;
	private String tarjetaCredito;
	private String ultimosCuatroDigitos;
	private String ejercidoCreditoHipotecario;
	private String ejercidoCreditoAutomotriz;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getTipoReporte() {
		return tipoReporte;
	}

	public void setTipoReporte(String tipoReporte) {
		this.tipoReporte = tipoReporte;
	}

	public String getTipoSalida() {
		return tipoSalida;
	}

	public void setTipoSalida(String tipoSalida) {
		this.tipoSalida = tipoSalida;
	}

	public String getReferenciaOperador() {
		return referenciaOperador;
	}

	public void setReferenciaOperador(String referenciaOperador) {
		this.referenciaOperador = referenciaOperador;
	}

	public String getTarjetaCredito() {
		return tarjetaCredito;
	}

	public void setTarjetaCredito(String tarjetaCredito) {
		this.tarjetaCredito = tarjetaCredito;
	}

	public String getUltimosCuatroDigitos() {
		return ultimosCuatroDigitos;
	}

	public void setUltimosCuatroDigitos(String ultimosCuatroDigitos) {
		this.ultimosCuatroDigitos = ultimosCuatroDigitos;
	}

	public String getEjercidoCreditoHipotecario() {
		return ejercidoCreditoHipotecario;
	}

	public void setEjercidoCreditoHipotecario(String ejercidoCreditoHipotecario) {
		this.ejercidoCreditoHipotecario = ejercidoCreditoHipotecario;
	}

	public String getEjercidoCreditoAutomotriz() {
		return ejercidoCreditoAutomotriz;
	}

	public void setEjercidoCreditoAutomotriz(String ejercidoCreditoAutomotriz) {
		this.ejercidoCreditoAutomotriz = ejercidoCreditoAutomotriz;
	}

	@Override
	public String toString() {
		return "INAutentica [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", tipoReporte=" + tipoReporte
				+ ", tipoSalida=" + tipoSalida + ", referenciaOperador=" + referenciaOperador + ", tarjetaCredito="
				+ tarjetaCredito + ", ultimosCuatroDigitos=" + ultimosCuatroDigitos + ", ejercidoCreditoHipotecario="
				+ ejercidoCreditoHipotecario + ", ejercidoCreditoAutomotriz=" + ejercidoCreditoAutomotriz + "]";
	}

	
}
