package com.mx.finsol.informegerente.model.entity.burocredito;

public class OUTNombreBean {

	
	private String etiquetaEncabezadoSegmento;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String apellidoAdicional;
	private String primerNombre;
	private String segundoNombre;
	private String fechaNacimiento;
	private String rfc;
	private String prefijoPersona;
	private String sufijoPersonal;
	private String nacionalidad;
	private String tipoResidencia;
	private String numeroLicencia;
	private String estadoCivil;
	private String genero;
	private String numeroCedulaProfesional;
	private String numeroRegistroElectoral;
	private String claveIdentificacionUnica;
	private String clavePais;
	private String numeroDependientes;
	private String edadesDependientes;
	private String fechaRecepcionInformacionDependientes;
	private String fechaDefuncion;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getApellidoAdicional() {
		return apellidoAdicional;
	}

	public void setApellidoAdicional(String apellidoAdicional) {
		this.apellidoAdicional = apellidoAdicional;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getPrefijoPersona() {
		return prefijoPersona;
	}

	public void setPrefijoPersona(String prefijoPersona) {
		this.prefijoPersona = prefijoPersona;
	}

	public String getSufijoPersonal() {
		return sufijoPersonal;
	}

	public void setSufijoPersonal(String sufijoPersonal) {
		this.sufijoPersonal = sufijoPersonal;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getTipoResidencia() {
		return tipoResidencia;
	}

	public void setTipoResidencia(String tipoResidencia) {
		this.tipoResidencia = tipoResidencia;
	}

	public String getNumeroLicencia() {
		return numeroLicencia;
	}

	public void setNumeroLicencia(String numeroLicencia) {
		this.numeroLicencia = numeroLicencia;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getNumeroCedulaProfesional() {
		return numeroCedulaProfesional;
	}

	public void setNumeroCedulaProfesional(String numeroCedulaProfesional) {
		this.numeroCedulaProfesional = numeroCedulaProfesional;
	}

	public String getNumeroRegistroElectoral() {
		return numeroRegistroElectoral;
	}

	public void setNumeroRegistroElectoral(String numeroRegistroElectoral) {
		this.numeroRegistroElectoral = numeroRegistroElectoral;
	}

	public String getClaveIdentificacionUnica() {
		return claveIdentificacionUnica;
	}

	public void setClaveIdentificacionUnica(String claveIdentificacionUnica) {
		this.claveIdentificacionUnica = claveIdentificacionUnica;
	}

	public String getClavePais() {
		return clavePais;
	}

	public void setClavePais(String clavePais) {
		this.clavePais = clavePais;
	}

	public String getNumeroDependientes() {
		return numeroDependientes;
	}

	public void setNumeroDependientes(String numeroDependientes) {
		this.numeroDependientes = numeroDependientes;
	}

	public String getEdadesDependientes() {
		return edadesDependientes;
	}

	public void setEdadesDependientes(String edadesDependientes) {
		this.edadesDependientes = edadesDependientes;
	}

	public String getFechaRecepcionInformacionDependientes() {
		return fechaRecepcionInformacionDependientes;
	}

	public void setFechaRecepcionInformacionDependientes(String fechaRecepcionInformacionDependientes) {
		this.fechaRecepcionInformacionDependientes = fechaRecepcionInformacionDependientes;
	}

	public String getFechaDefuncion() {
		return fechaDefuncion;
	}

	public void setFechaDefuncion(String fechaDefuncion) {
		this.fechaDefuncion = fechaDefuncion;
	}

	@Override
	public String toString() {
		return "OUTNombreBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", apellidoPaterno="
				+ apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno + ", apellidoAdicional=" + apellidoAdicional
				+ ", primerNombre=" + primerNombre + ", segundoNombre=" + segundoNombre + ", fechaNacimiento="
				+ fechaNacimiento + ", rfc=" + rfc + ", prefijoPersona=" + prefijoPersona + ", sufijoPersonal="
				+ sufijoPersonal + ", nacionalidad=" + nacionalidad + ", tipoResidencia=" + tipoResidencia
				+ ", numeroLicencia=" + numeroLicencia + ", estadoCivil=" + estadoCivil + ", genero=" + genero
				+ ", numeroCedulaProfesional=" + numeroCedulaProfesional + ", numeroRegistroElectoral="
				+ numeroRegistroElectoral + ", claveIdentificacionUnica=" + claveIdentificacionUnica + ", clavePais="
				+ clavePais + ", numeroDependientes=" + numeroDependientes + ", edadesDependientes="
				+ edadesDependientes + ", fechaRecepcionInformacionDependientes="
				+ fechaRecepcionInformacionDependientes + ", fechaDefuncion=" + fechaDefuncion + "]";
	}
}
