package com.mx.finsol.informegerente.model.entity.solicitudes;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BitacoraAtomosSolicitudes implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer idBitacoraAtomoSolicitudes;
	private Integer folio;
	private String solInicial;
	private String solFinal;
	private String solicitud;
	private Double latitud;
	private Double longitud;
	private String usuarioAlta;
	private String usuarioUltMod;
	@JsonFormat(timezone = "America/Mexico_City")
	private Date fechaAlta;
	@JsonFormat(timezone = "America/Mexico_City")
	private Date fechaUltMod;
	private Integer idCatEstatus;

}
