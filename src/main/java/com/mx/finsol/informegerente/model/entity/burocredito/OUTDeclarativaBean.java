package com.mx.finsol.informegerente.model.entity.burocredito;

public class OUTDeclarativaBean {

	
	private String etiquetaEncabezadoSegmento;
	private String tipoSegmento;
	private String declarativaCliente;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getTipoSegmento() {
		return tipoSegmento;
	}

	public void setTipoSegmento(String tipoSegmento) {
		this.tipoSegmento = tipoSegmento;
	}

	public String getDeclarativaCliente() {
		return declarativaCliente;
	}

	public void setDeclarativaCliente(String declarativaCliente) {
		this.declarativaCliente = declarativaCliente;
	}

	@Override
	public String toString() {
		return "OUTDeclarativaBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", tipoSegmento="
				+ tipoSegmento + ", declarativaCliente=" + declarativaCliente + "]";
	}

}
