package com.mx.finsol.informegerente.model.entity.burocredito;

public class INCuentaBean {

	private String etiquetaEncabezadoSegmento;
	private String numeroCuenta;
	private String claveUsuario;
	private String nombreUsuario;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getClaveUsuario() {
		return claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	@Override
	public String toString() {
		return "INCuentaBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", numeroCuenta="
				+ numeroCuenta + ", claveUsuario=" + claveUsuario + ", nombreUsuario=" + nombreUsuario + "]";
	}

}
