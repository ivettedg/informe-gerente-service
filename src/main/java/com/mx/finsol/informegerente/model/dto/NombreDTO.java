package com.mx.finsol.informegerente.model.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NombreDTO {
	
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String apellidoAdicional;
	private String primerNombre;
	private String segundoNombre;
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City")
	private Date fechaNacimiento;
	private String rfc;
	private String prefijo;
	private String sufijo;
	private String nacionalidad;
	private String residencia;
	private String numeroLicenciaConducir;
	private String estadoCivil;
	private String sexo;
	private String numeroCedulaProfesional;
	private String numeroRegistroElectoral;
	private String claveImpuestosOtroPais;
	private String claveOtroPais;
	private String numeroDependientes;
	private String edadesDependientes;

}
