package com.mx.finsol.informegerente.model.entity.burocredito;

public class OutScoreBean {

	
	private String etiquetaEncabezadoSegmento;
	private String nombreScore;
	private String codigoScore;
	private String valorScore;
	private String codigoRazon;
	private String codigoRazon2;
	private String codigoRazon3;
	private String codigoError;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getNombreScore() {
		return nombreScore;
	}

	public void setNombreScore(String nombreScore) {
		this.nombreScore = nombreScore;
	}

	public String getCodigoScore() {
		return codigoScore;
	}

	public void setCodigoScore(String codigoScore) {
		this.codigoScore = codigoScore;
	}

	public String getValorScore() {
		return valorScore;
	}

	public void setValorScore(String valorScore) {
		this.valorScore = valorScore;
	}

	public String getCodigoRazon() {
		return codigoRazon;
	}

	public void setCodigoRazon(String codigoRazon) {
		this.codigoRazon = codigoRazon;
	}

	public String getCodigoRazon2() {
		return codigoRazon2;
	}

	public void setCodigoRazon2(String codigoRazon2) {
		this.codigoRazon2 = codigoRazon2;
	}

	public String getCodigoRazon3() {
		return codigoRazon3;
	}

	public void setCodigoRazon3(String codigoRazon3) {
		this.codigoRazon3 = codigoRazon3;
	}

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	@Override
	public String toString() {
		return "OutScoreBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", nombreScore=" + nombreScore
				+ ", codigoScore=" + codigoScore + ", valorScore=" + valorScore + ", codigoRazon=" + codigoRazon
				+ ", codigoRazon2=" + codigoRazon2 + ", codigoRazon3=" + codigoRazon3 + ", codigoError=" + codigoError
				+ "]";
	}
}
