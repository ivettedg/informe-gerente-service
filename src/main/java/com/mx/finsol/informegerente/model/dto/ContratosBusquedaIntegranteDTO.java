package com.mx.finsol.informegerente.model.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContratosBusquedaIntegranteDTO {

	private String contrato;
	private List<String> integrantes;
	private String rol;
	
		
	
}
