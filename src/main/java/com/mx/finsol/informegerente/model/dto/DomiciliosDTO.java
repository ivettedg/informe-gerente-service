package com.mx.finsol.informegerente.model.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DomiciliosDTO {

	private String direccion1;
	private String direccion2;
	private String coloniaPoblacion;
	private String delegacionMunicipio;
	private String ciudad;
	private String estado;
	private String cp;
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City")
	private Date fechaResidencia;
	private String numeroTelefono;
	private String extension;
	private String fax;
	private String tipoDomicilio;
	private String indicadorEspecialDomicilio;
	private String codPais;

	
}
