package com.mx.finsol.informegerente.model.entity.burocredito;

public class OUTEncabezadoBean {

	
	private String etiquetaSegmento;
	private String version;
	private String numeroReferenciaOperador;
	private String identificadorBuro;
	private String clavePais;
	private String reservadoReporteCreditoIntegrado;
	private String claveUsuario;
	private String claveRespuesta;
	private String reservado;

	public String getEtiquetaSegmento() {
		return etiquetaSegmento;
	}

	public void setEtiquetaSegmento(String etiquetaSegmento) {
		this.etiquetaSegmento = etiquetaSegmento;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getNumeroReferenciaOperador() {
		return numeroReferenciaOperador;
	}

	public void setNumeroReferenciaOperador(String numeroReferenciaOperador) {
		this.numeroReferenciaOperador = numeroReferenciaOperador;
	}

	public String getIdentificadorBuro() {
		return identificadorBuro;
	}

	public void setIdentificadorBuro(String identificadorBuro) {
		this.identificadorBuro = identificadorBuro;
	}

	public String getClavePais() {
		return clavePais;
	}

	public void setClavePais(String clavePais) {
		this.clavePais = clavePais;
	}

	public String getReservadoReporteCreditoIntegrado() {
		return reservadoReporteCreditoIntegrado;
	}

	public void setReservadoReporteCreditoIntegrado(String reservadoReporteCreditoIntegrado) {
		this.reservadoReporteCreditoIntegrado = reservadoReporteCreditoIntegrado;
	}

	public String getClaveUsuario() {
		return claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}

	public String getClaveRespuesta() {
		return claveRespuesta;
	}

	public void setClaveRespuesta(String claveRespuesta) {
		this.claveRespuesta = claveRespuesta;
	}

	public String getReservado() {
		return reservado;
	}

	public void setReservado(String reservado) {
		this.reservado = reservado;
	}

	@Override
	public String toString() {
		return "OUTEncabezadoBean [etiquetaSegmento=" + etiquetaSegmento + ", version=" + version
				+ ", numeroReferenciaOperador=" + numeroReferenciaOperador + ", identificadorBuro=" + identificadorBuro
				+ ", clavePais=" + clavePais + ", reservadoReporteCreditoIntegrado=" + reservadoReporteCreditoIntegrado
				+ ", claveUsuario=" + claveUsuario + ", claveRespuesta=" + claveRespuesta + ", reservado=" + reservado
				+ "]";
	}
}
