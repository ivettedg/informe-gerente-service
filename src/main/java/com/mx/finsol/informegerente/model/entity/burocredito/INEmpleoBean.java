package com.mx.finsol.informegerente.model.entity.burocredito;

public class INEmpleoBean {
	
	private String etiquetaEncabezadoSegmento;
	private String razonSocialEmpleador;
	private String primeraLineaDireccion;
	private String segundaLineaDireccion;
	private String coloniaPoblacion;
	private String delegacionMunicipio;
	private String ciudad;
	private String estado;
	private String codigoPostal;
	private String numeroTelefono;
	private String extensionTelefonica;
	private String numeroFax;
	private String cargoOcupacion;
	private String fechaContratacion;
	private String claveMonedaPagoSueldo;
	private String montoSueldoSalario;
	private String periodoPagoBaseSalarial;
	private String numeroEmpleado;
	private String fechaUltimoEmpleo;
	private String origenRazonSocialDomicilioPais;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getRazonSocialEmpleador() {
		return razonSocialEmpleador;
	}

	public void setRazonSocialEmpleador(String razonSocialEmpleador) {
		this.razonSocialEmpleador = razonSocialEmpleador;
	}

	public String getPrimeraLineaDireccion() {
		return primeraLineaDireccion;
	}

	public void setPrimeraLineaDireccion(String primeraLineaDireccion) {
		this.primeraLineaDireccion = primeraLineaDireccion;
	}

	public String getSegundaLineaDireccion() {
		return segundaLineaDireccion;
	}

	public void setSegundaLineaDireccion(String segundaLineaDireccion) {
		this.segundaLineaDireccion = segundaLineaDireccion;
	}

	public String getColoniaPoblacion() {
		return coloniaPoblacion;
	}

	public void setColoniaPoblacion(String coloniaPoblacion) {
		this.coloniaPoblacion = coloniaPoblacion;
	}

	public String getDelegacionMunicipio() {
		return delegacionMunicipio;
	}

	public void setDelegacionMunicipio(String delegacionMunicipio) {
		this.delegacionMunicipio = delegacionMunicipio;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getExtensionTelefonica() {
		return extensionTelefonica;
	}

	public void setExtensionTelefonica(String extensionTelefonica) {
		this.extensionTelefonica = extensionTelefonica;
	}

	public String getNumeroFax() {
		return numeroFax;
	}

	public void setNumeroFax(String numeroFax) {
		this.numeroFax = numeroFax;
	}

	public String getCargoOcupacion() {
		return cargoOcupacion;
	}

	public void setCargoOcupacion(String cargoOcupacion) {
		this.cargoOcupacion = cargoOcupacion;
	}

	public String getFechaContratacion() {
		return fechaContratacion;
	}

	public void setFechaContratacion(String fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	public String getClaveMonedaPagoSueldo() {
		return claveMonedaPagoSueldo;
	}

	public void setClaveMonedaPagoSueldo(String claveMonedaPagoSueldo) {
		this.claveMonedaPagoSueldo = claveMonedaPagoSueldo;
	}

	public String getMontoSueldoSalario() {
		return montoSueldoSalario;
	}

	public void setMontoSueldoSalario(String montoSueldoSalario) {
		this.montoSueldoSalario = montoSueldoSalario;
	}

	public String getPeriodoPagoBaseSalarial() {
		return periodoPagoBaseSalarial;
	}

	public void setPeriodoPagoBaseSalarial(String periodoPagoBaseSalarial) {
		this.periodoPagoBaseSalarial = periodoPagoBaseSalarial;
	}

	public String getNumeroEmpleado() {
		return numeroEmpleado;
	}

	public void setNumeroEmpleado(String numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}

	public String getFechaUltimoEmpleo() {
		return fechaUltimoEmpleo;
	}

	public void setFechaUltimoEmpleo(String fechaUltimoEmpleo) {
		this.fechaUltimoEmpleo = fechaUltimoEmpleo;
	}

	public String getOrigenRazonSocialDomicilioPais() {
		return origenRazonSocialDomicilioPais;
	}

	public void setOrigenRazonSocialDomicilioPais(String origenRazonSocialDomicilioPais) {
		this.origenRazonSocialDomicilioPais = origenRazonSocialDomicilioPais;
	}

	@Override
	public String toString() {
		return "INEmpleoBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", razonSocialEmpleador="
				+ razonSocialEmpleador + ", primeraLineaDireccion=" + primeraLineaDireccion + ", segundaLineaDireccion="
				+ segundaLineaDireccion + ", coloniaPoblacion=" + coloniaPoblacion + ", delegacionMunicipio="
				+ delegacionMunicipio + ", ciudad=" + ciudad + ", estado=" + estado + ", codigoPostal=" + codigoPostal
				+ ", numeroTelefono=" + numeroTelefono + ", extensionTelefonica=" + extensionTelefonica + ", numeroFax="
				+ numeroFax + ", cargoOcupacion=" + cargoOcupacion + ", fechaContratacion=" + fechaContratacion
				+ ", claveMonedaPagoSueldo=" + claveMonedaPagoSueldo + ", montoSueldoSalario=" + montoSueldoSalario
				+ ", periodoPagoBaseSalarial=" + periodoPagoBaseSalarial + ", numeroEmpleado=" + numeroEmpleado
				+ ", fechaUltimoEmpleo=" + fechaUltimoEmpleo + ", origenRazonSocialDomicilioPais="
				+ origenRazonSocialDomicilioPais + "]";
	}

}
