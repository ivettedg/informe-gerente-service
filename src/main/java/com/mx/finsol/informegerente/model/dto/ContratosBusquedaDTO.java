package com.mx.finsol.informegerente.model.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContratosBusquedaDTO {

	
	private String cliente;
	private List<String> contratos;
	private String solicitud;
	
}
