package com.mx.finsol.informegerente.model.entity.contratos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "contratos", catalog = "credprod_finsol", schema = "dbo")
@XmlRootElement
public class Contratos implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull(message = "El campo contrato en Contratos no puede ser nulo.")
	@Size(message = "El campo contrato en Contratos no puede ser mayor a 8 caracteres.", max = 8)
	@Column(name = "contrato")
	private String contrato;

	@NotNull(message = "El campo solicitud en Contratos no puede ser nulo.")
	@Size(message = "El campo solicitud en Contratos no puede ser mayor a 8 caracteres.", max = 8)
	@Column(name = "solicitud")
	private String solicitud;

	@NotNull(message = "El campo cliente en Contratos no puede ser nulo.")
	@Size(message = "El campo cliente en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "cliente")
	private String cliente;

	@NotNull(message = "El campo sucursal en Contratos no puede ser nulo.")
	@Size(message = "El campo sucursal en Contratos no debe ser mayor a 10 caracteres.", max = 10)
	@Column(name = "sucursal")
	private Integer sucursal;

	@NotNull(message = "El campo vendedor en Contratos no puede ser nulo.")
	@Size(message = "El campo vendedor en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "vendedor")
	private String vendedor;

	@NotNull(message = "El campo tipoCredito en Contratos no puede ser nulo.")
	@Column(name = "tipo_credito")
	private Character tipoCredito;

	@NotNull(message = "El campo tipoDisposicion en Contratos no puede ser nulo.")
	@Column(name = "tipo_disposicion")
	private Character tipoDisposicion;

	@NotNull(message = "El campo fechaContrato en Contratos no puede ser nulo.")
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_contrato")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaContrato;

	@Digits(integer = 10, message = "El campo cheque en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "cheque")
	private Integer cheque;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_cheque")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCheque;

	@Digits(integer = 3, message = "El campo banco en Contratos no puede contener mas de 3 digitos.", fraction = 0)
	@Column(name = "banco")
	private Short banco;

	@Size(message = "El campo cuenta en Contratos no debe ser mayor a 24 caracteres.", max = 24)
	@Column(name = "cuenta")
	private String cuenta;

	@NotNull(message = "El campo personalidad en Contratos no puede ser nulo.")
	@Column(name = "personalidad")
	private Character personalidad;

	@NotNull(message = "El campo convenio en Contratos no puede ser nulo.")
	@Digits(integer = 5, message = "El campo convenio en Contratos no puede contener mas de 5 digitos.", fraction = 0)
	@Column(name = "convenio")
	private Short convenio;

	@NotNull(message = "El campo promocion en Contratos no puede ser nulo.")
	@Size(message = "El campo promocion en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "promocion")
	private String promocion;

	@NotNull(message = "El campo fechaDisposicion en Contratos no puede ser nulo.")
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_disposicion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaDisposicion;

	@NotNull(message = "El campo efectivo en Contratos no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo efectivo en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "efectivo")
	private BigDecimal efectivo;

	@NotNull(message = "El campo monto en Contratos no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo monto en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto")
	private BigDecimal monto;

	@NotNull(message = "El campo montoPago en Contratos no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo montoPago en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_pago")
	private BigDecimal montoPago;

	@NotNull(message = "El campo montoUltPago en Contratos no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo montoUltPago en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_ult_pago")
	private BigDecimal montoUltPago;

	@NotNull(message = "El campo noPagos en Contratos no puede ser nulo.")
	@Digits(integer = 3, message = "El campo noPagos en Contratos no puede contener mas de 3 digitos.", fraction = 0)
	@Column(name = "no_pagos")
	private Short noPagos;

	@NotNull(message = "El campo tasa en Contratos no puede ser nulo.")
	@Column(name = "tasa")
	private Double tasa;

	@NotNull(message = "El campo frecuenciaPago en Contratos no puede ser nulo.")
	@Column(name = "frecuencia_pago")
	private Character frecuenciaPago;

	@NotNull(message = "El campo fechaAnclaPago en Contratos no puede ser nulo.")
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_ancla_pago")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAnclaPago;

	@NotNull(message = "El campo fechaPrimVenc en Contratos no puede ser nulo.")
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_prim_venc")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaPrimVenc;

	@NotNull(message = "El campo gastosAdmon en Contratos no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo gastosAdmon en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "gastos_admon")
	private BigDecimal gastosAdmon;

	@NotNull(message = "El campo pctIvaGastosAdmon en Contratos no puede ser nulo.")
	@Column(name = "pct_iva_gastos_admon")
	private Double pctIvaGastosAdmon;

	@NotNull(message = "El campo comisionApertura en Contratos no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo comisionApertura en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "comision_apertura")
	private BigDecimal comisionApertura;

	@NotNull(message = "El campo pctIvaComisionAper en Contratos no puede ser nulo.")
	@Column(name = "pct_iva_comision_aper")
	private Double pctIvaComisionAper;

	@NotNull(message = "El campo seguro en Contratos no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo seguro en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "seguro")
	private BigDecimal seguro;

	@NotNull(message = "El campo pctIvaSeguro en Contratos no puede ser nulo.")
	@Column(name = "pct_iva_seguro")
	private Double pctIvaSeguro;

	@Size(message = "El campo codMonto1 en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "cod_monto_1")
	private String codMonto1;

	@Digits(integer = 19, fraction = 4, message = "El campo monto1 en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_1")
	private BigDecimal monto1;

	@Column(name = "pct_iva_monto_1")
	private Double pctIvaMonto1;

	@Size(message = "El campo codMonto2 en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "cod_monto_2")
	private String codMonto2;

	@Digits(integer = 19, fraction = 4, message = "El campo monto2 en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_2")
	private BigDecimal monto2;

	@Column(name = "pct_iva_monto_2")
	private Double pctIvaMonto2;

	@NotNull(message = "El campo analista en Contratos no puede ser nulo.")
	@Size(message = "El campo analista en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "analista")
	private String analista;

	@Size(message = "El campo gestor en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "gestor")
	private String gestor;

	@NotNull(message = "El campo noRetrasos en Contratos no puede ser nulo.")
	@Digits(integer = 5, message = "El campo noRetrasos en Contratos no puede contener mas de 5 digitos.", fraction = 0)
	@Column(name = "no_retrasos")
	private Short noRetrasos;

	@NotNull(message = "El campo maximoRetraso en Contratos no puede ser nulo.")
	@Digits(integer = 5, message = "El campo maximoRetraso en Contratos no puede contener mas de 5 digitos.", fraction = 0)
	@Column(name = "maximo_retraso")
	private Short maximoRetraso;

	@NotNull(message = "El campo promedioRetraso en Contratos no puede ser nulo.")
	@Column(name = "promedio_retraso")
	private Double promedioRetraso;

	@NotNull(message = "El campo fechaProxVenc en Contratos no puede ser nulo.")
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_prox_venc")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaProxVenc;

	@NotNull(message = "El campo fechaProxPago en Contratos no puede ser nulo.")
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_prox_pago")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaProxPago;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_ult_pago")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltPago;

	@NotNull(message = "El campo noUltPago en Contratos no puede ser nulo.")
	@Digits(integer = 5, message = "El campo noUltPago en Contratos no puede contener mas de 5 digitos.", fraction = 0)
	@Column(name = "no_ult_pago")
	private Short noUltPago;

	@NotNull(message = "El campo capitalInsoluto en Contratos no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo capitalInsoluto en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "capital_insoluto")
	private BigDecimal capitalInsoluto;

	@NotNull(message = "El campo statusCobranza en Contratos no puede ser nulo.")
	@Column(name = "status_cobranza")
	private Character statusCobranza;

	@NotNull(message = "El campo status en Contratos no puede ser nulo.")
	@Column(name = "status")
	private Character status;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_corte")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCorte;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_car_ven")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCarVen;

	@Column(name = "fecha_esp_liq")
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEspLiq;

	@Column(name = "fecha_liq")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaLiq;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_ult_mov")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltMov;

	@Size(message = "El campo producto en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "producto")
	private String producto;

	@Column(name = "pct_finan")
	private Double pctFinan;

	@Size(message = "El campo propietario en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "propietario")
	private String propietario;

	@Size(message = "El campo garantiza en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "garantiza")
	private String garantiza;

	@Digits(integer = 19, fraction = 4, message = "El campo montoCastigo en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_castigo")
	private BigDecimal montoCastigo;

	@Digits(integer = 19, fraction = 4, message = "El campo montoReservas en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_reservas")
	private BigDecimal montoReservas;

	@Size(message = "El campo responsablePago en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "responsable_pago")
	private String responsablePago;

	@Digits(integer = 10, message = "El campo noConsecutivo en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "no_consecutivo")
	private Integer noConsecutivo;

	@Size(message = "El campo vendedorOrig en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "vendedor_orig")
	private String vendedorOrig;

	@Size(message = "El campo origenRecursos en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "origen_recursos")
	private String origenRecursos;

	@Size(message = "El campo aval1 en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "aval_1")
	private String aval1;

	@Size(message = "El campo aval2 en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "aval_2")
	private String aval2;

	@Column(name = "status_cv")
	private Character statusCv;

	@Column(name = "razon_salida")
	private Character razonSalida;

	@Digits(integer = 10, message = "El campo mesesPagoSost en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "meses_pago_sost")
	private Integer mesesPagoSost;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_calculo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCalculo;

	@Size(message = "El campo nombre en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "gte_ventas")
	private String gteVentas;

	@Digits(integer = 19, fraction = 4, message = "El campo montoPromocion en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_promocion")
	private BigDecimal montoPromocion;

	@Digits(integer = 19, fraction = 4, message = "El campo montoLiqCtrant en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_liq_ctrant")
	private BigDecimal montoLiqCtrant;

	@Digits(integer = 5, message = "El campo grupo en Contratos no puede contener mas de 5 digitos.", fraction = 0)
	@Column(name = "grupo")
	private Short grupo;


	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_reembolso")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaReembolso;

	@Digits(integer = 10, message = "El campo lugarDisposicion en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "lugar_disposicion")
	private Integer lugarDisposicion;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_impresion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaImpresion;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "hora_disposicion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date horaDisposicion;

	@Digits(integer = 19, fraction = 4, message = "El campo limiteCredito en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "limite_credito")
	private BigDecimal limiteCredito;

	@Digits(integer = 19, fraction = 4, message = "El campo disponible en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "disponible")
	private BigDecimal disponible;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_ult_disposicion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltDisposicion;

	@Digits(integer = 19, fraction = 4, message = "El campo interesXPagar en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "interes_x_pagar")
	private BigDecimal interesXPagar;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaInteresXPagar en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_interes_x_pagar")
	private BigDecimal ivaInteresXPagar;

	@Digits(integer = 19, fraction = 4, message = "El campo interesPeriodo en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "interes_periodo")
	private BigDecimal interesPeriodo;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaInteresPeriodo en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_interes_periodo")
	private BigDecimal ivaInteresPeriodo;

	@Digits(integer = 19, fraction = 4, message = "El campo capitalPagadoParcial en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "capital_pagado_parcial")
	private BigDecimal capitalPagadoParcial;

	@Digits(integer = 19, fraction = 4, message = "El campo interesPagadoParcial en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "interes_pagado_parcial")
	private BigDecimal interesPagadoParcial;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaInteresPagadoParcial en Contratos no puede contener mas de 10 digitos.")
	@Column(name = "iva_interes_pagado_parcial")
	private BigDecimal ivaInteresPagadoParcial;

	@Digits(integer = 19, fraction = 4, message = "El campo devengadoMensual en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "devengado_mensual")
	private BigDecimal devengadoMensual;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevengadoMensual en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_devengado_mensual")
	private BigDecimal ivaDevengadoMensual;

	@Digits(integer = 19, fraction = 4, message = "El campo devengadoMesAnterior en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "devengado_mes_anterior")
	private BigDecimal devengadoMesAnterior;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_calculo_interes")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaCalculoInteres;

	@Digits(integer = 19, fraction = 4, message = "El campo saldoMesAnterior en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "saldo_mes_anterior")
	private BigDecimal saldoMesAnterior;

	@Column(name = "acum_efectivo_disp")
	private Double acumEfectivoDisp;

	@Column(name = "acum_comision_disp")
	private Double acumComisionDisp;

	@Column(name = "iva_devengado_mes_ant")
	private Double ivaDevengadoMesAnt;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_activ")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaActiv;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_act_saldo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaActSaldo;

	@Digits(integer = 10, message = "El campo noPagosVencidos en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "no_pagos_vencidos")
	private Integer noPagosVencidos;

	@Digits(integer = 19, fraction = 4, message = "El campo saldoCapital en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "saldo_capital")
	private BigDecimal saldoCapital;

	@Digits(integer = 19, fraction = 4, message = "El campo saldoIntPeriodo en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "saldo_int_periodo")
	private BigDecimal saldoIntPeriodo;

	@Digits(integer = 19, fraction = 4, message = "El campo saldoIvaIntPeriodo en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "saldo_iva_int_periodo")
	private BigDecimal saldoIvaIntPeriodo;

	@Digits(integer = 19, fraction = 4, message = "El campo saldoMoratorios en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "saldo_moratorios")
	private BigDecimal saldoMoratorios;

	@Digits(integer = 19, fraction = 4, message = "El campo saldoIvaMoratorios en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "saldo_iva_moratorios")
	private BigDecimal saldoIvaMoratorios;

	@Digits(integer = 19, fraction = 4, message = "El campo saldoGastosCobranza en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "saldo_gastos_cobranza")
	private BigDecimal saldoGastosCobranza;

	@Digits(integer = 19, fraction = 4, message = "El campo saldoIvaGastosCobranza en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "saldo_iva_gastos_cobranza")
	private BigDecimal saldoIvaGastosCobranza;

	@Size(message = "El campo nombre en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "fpd")
	private String fpd;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_ult_aniv")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltAniv;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_prox_aniv")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaProxAniv;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_inactiv")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaInactiv;

	@Digits(integer = 10, message = "El campo calificacion en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "calificacion")
	private Integer calificacion;

	@Size(message = "El campo nivelRiesgo en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "nivel_riesgo")
	private String nivelRiesgo;

	@Digits(integer = 10, message = "El campo minPagosVenc en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "min_pagos_venc")
	private Integer minPagosVenc;

	@Digits(integer = 10, message = "El campo minDiasVenc en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "min_dias_venc")
	private Integer minDiasVenc;

	@Column(name = "prom_pagos_venc")
	private Double promPagosVenc;

	@Column(name = "prom_dias_venc")
	private Double promDiasVenc;

	@Digits(integer = 10, message = "El campo tendPagoVenc en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "tend_pago_venc")
	private Integer tendPagoVenc;

	@Column(name = "tipo_cuenta_cob")
	private Character tipoCuentaCob;

	@Digits(integer = 3, message = "El campo segmentoCob en Contratos no puede contener mas de 3 digitos.", fraction = 0)
	@Column(name = "segmento_cob")
	private Short segmentoCob;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_asig_cob")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAsigCob;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_act_riesgo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaActRiesgo;

	@Size(message = "El campo nivelGestion en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "nivel_gestion")
	private String nivelGestion;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_sale_cob")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaSaleCob;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_prim_disposicion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaPrimDisposicion;

	@Column(name = "comision_pagada")
	private Character comisionPagada;

	@Size(message = "El campo tipoVerificacionFisicaDom en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "tipo_verificacion_fisica_dom")
	private String tipoVerificacionFisicaDom;

	@Size(message = "El campo tipoVerificacionFisicaEmp en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "tipo_verificacion_fisica_emp")
	private String tipoVerificacionFisicaEmp;

	@Size(message = "El campo tipoVerificacionReferencias en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "tipo_verificacion_referencias")
	private String tipoVerificacionReferencias;

	@Size(message = "El campo contratoRecomienda en Contratos no debe ser mayor a 8 caracteres.", max = 8)
	@Column(name = "contrato_recomienda")
	private String contratoRecomienda;

	@Column(name = "origen_credito")
	private Character origenCredito;

	@Column(name = "asignado_gestion")
	private Character asignadoGestion;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_asig_gestion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAsigGestion;

	@Size(message = "El campo motivoNoAsigGestion en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "motivo_no_asig_gestion")
	private String motivoNoAsigGestion;

	@Column(name = "fecha_inactiva_gestion")
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaInactivaGestion;

	@Digits(integer = 19, fraction = 4, message = "El campo saldoCapitalVencido en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "saldo_capital_vencido")
	private BigDecimal saldoCapitalVencido;

	@NotNull(message = "El campo expedienteCentralizado en Contratos no puede ser nulo.")
	@Column(name = "expediente_centralizado")
	private Character expedienteCentralizado;

	@Digits(integer = 10, message = "El campo mesActDev en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "mes_act_dev")
	private Integer mesActDev;

	@Size(message = "El campo codResoForzado en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "cod_reso_forzado")
	private String codResoForzado;

	@Digits(integer = 19, fraction = 4, message = "El campo flujoMes en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "flujo_mes")
	private BigDecimal flujoMes;

	@Digits(integer = 19, fraction = 4, message = "El campo flujoMesAnterior en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "flujo_mes_anterior")
	private BigDecimal flujoMesAnterior;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_act_flujo_mes")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaActFlujoMes;

	@Digits(integer = 19, fraction = 4, message = "El campo capitalVencidoMesAnterior en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "capital_vencido_mes_anterior")
	private BigDecimal capitalVencidoMesAnterior;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_ult_atraso")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltAtraso;

	@Digits(integer = 19, fraction = 4, message = "El campo montoAhorro en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_ahorro")
	private BigDecimal montoAhorro;

	@Digits(integer = 19, fraction = 4, message = "El campo montoSubsidio en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_subsidio")
	private BigDecimal montoSubsidio;

	@Digits(integer = 19, fraction = 4, message = "El campo montoProyecto en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_proyecto")
	private BigDecimal montoProyecto;

	@Digits(integer = 8, message = "El campo folioApartadoConavi en Contratos no puede contener mas de 8 digitos.", fraction = 0)
	@Column(name = "folio_apartado_conavi")
	private Integer folioApartadoConavi;

	@Digits(integer = 19, fraction = 4, message = "El campo montoMaterial en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_material")
	private BigDecimal montoMaterial;

	@Digits(integer = 10, message = "El campo sucursalCobranza en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "sucursal_cobranza")
	private Integer sucursalCobranza;

	@NotNull(message = "El campo acumDisposiciones en Contratos no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo acumDisposiciones en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "acum_disposiciones")
	private BigDecimal acumDisposiciones;

	@NotNull(message = "El campo lineaShf en Contratos no puede ser nulo.")
	@Digits(integer = 3, message = "El campo lineaShf en Contratos no puede contener mas de 3 digitos.", fraction = 0)
	@Column(name = "linea_shf")
	private Short lineaShf;

	@Column(name = "status_buro")
	private Character statusBuro;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_rep_buro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRepBuro;

	@Size(message = "El campo codErrBuro en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "cod_err_buro")
	private String codErrBuro;

	@NotNull(message = "El campo mesComisionPagadaVend en Contratos no puede ser nulo.")
	@Digits(integer = 10, message = "El campo mesComisionPagadaVend en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "mes_comision_pagada_vend")
	private Integer mesComisionPagadaVend;

	@NotNull(message = "El campo comisionPagadaVend en Contratos no puede ser nulo.")
	@Column(name = "comision_pagada_vend")
	private Character comisionPagadaVend;

	@Digits(integer = 19, fraction = 4, message = "El campo devDia en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_dia")
	private BigDecimal devDia;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevDia en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_dev_dia")
	private BigDecimal ivaDevDia;

	@Digits(integer = 19, fraction = 4, message = "El campo devDiaReal en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_dia_real")
	private BigDecimal devDiaReal;

	@Digits(integer = 19, fraction = 4, message = "El campo devOperAcum en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_oper_acum")
	private BigDecimal devOperAcum;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevOperAcum en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_dev_oper_acum")
	private BigDecimal ivaDevOperAcum;

	@Digits(integer = 19, fraction = 4, message = "El campo devContMes en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_cont_mes")
	private BigDecimal devContMes;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevContMes en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_dev_cont_mes")
	private BigDecimal ivaDevContMes;

	@Digits(integer = 19, fraction = 4, message = "El campo devContMant en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_cont_mant")
	private BigDecimal devContMant;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevContMant en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_dev_cont_mant")
	private BigDecimal ivaDevContMant;

	@Digits(integer = 19, fraction = 4, message = "El campo devMorDia en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_mor_dia")
	private BigDecimal devMorDia;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevMorDia en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_dev_mor_dia")
	private BigDecimal ivaDevMorDia;

	@Digits(integer = 19, fraction = 4, message = "El campo devMorDiaReal en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_mor_dia_real")
	private BigDecimal devMorDiaReal;

	@Digits(integer = 19, fraction = 4, message = "El campo devMorOperAcum en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_mor_oper_acum")
	private BigDecimal devMorOperAcum;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevMorOperAcum en Contratos no puede contener mas de 10 digitos.")
	@Column(name = "iva_dev_mor_oper_acum")
	private BigDecimal ivaDevMorOperAcum;

	@Digits(integer = 19, fraction = 4, message = "El campo devMorContMes en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_mor_cont_mes")
	private BigDecimal devMorContMes;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevMorContMes en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_dev_mor_cont_mes")
	private BigDecimal ivaDevMorContMes;

	@Digits(integer = 19, fraction = 4, message = "El campo devMorContMant en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_mor_cont_mant")
	private BigDecimal devMorContMant;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevMorContMant en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_dev_mor_cont_mant")
	private BigDecimal ivaDevMorContMant;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_prox_pago_dg")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaProxPagoDg;

	@Column(name = "pagare_total_cred")
	private Character pagareTotalCred;

	@NotNull(message = "El campo calificacionAumLcr en Contratos no puede ser nulo.")
	@Digits(integer = 10, message = "El campo calificacionAumLcr en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "calificacion_aum_lcr")
	private Integer calificacionAumLcr;

	@Size(message = "El campo gteSucursal en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "gte_sucursal")
	private String gteSucursal;

	@Size(message = "El campo agenteTelefonico en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "agente_telefonico")
	private String agenteTelefonico;

	@Size(message = "El campo contacto en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "contacto")
	private String contacto;

	@Digits(integer = 10, message = "El campo calificacionFinal en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "calificacion_final")
	private Integer calificacionFinal;

	@Size(message = "El campo talonarioPagos en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "talonario_pagos")
	private String talonarioPagos;

	@NotNull(message = "El campo grupoRenovacion en Contratos no puede ser nulo.")
	@Digits(integer = 5, message = "El campo grupoRenovacion en Contratos no puede contener mas de 5 digitos.", fraction = 0)
	@Column(name = "grupo_renovacion")
	private Short grupoRenovacion;

	@NotNull(message = "El campo nivelCliente en Contratos no puede ser nulo.")
	@Digits(integer = 5, message = "El campo nivelCliente en Contratos no puede contener mas de 5 digitos.", fraction = 0)
	@Column(name = "nivel_cliente")
	private Short nivelCliente;

	@Digits(integer = 19, fraction = 4, message = "El campo montoComprometido en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_comprometido")
	private BigDecimal montoComprometido;

	@Size(message = "El campo claveEmpresa en Contratos no debe ser mayor a 9 caracteres.", max = 9)
	@Column(name = "clave_empresa")
	private String claveEmpresa;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_prim_incump_bc")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaPrimIncumpBc;

	@Digits(integer = 19, fraction = 4, message = "El campo montoSeguro en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_seguro")
	private BigDecimal montoSeguro;

	@Digits(integer = 19, fraction = 4, message = "El campo montoSalvoBuenCobro en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_salvo_buen_cobro")
	private BigDecimal montoSalvoBuenCobro;

	@Digits(integer = 19, fraction = 4, message = "El campo pagosPendientes en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "pagos_pendientes")
	private BigDecimal pagosPendientes;

	@Digits(integer = 19, fraction = 4, message = "El campo interesPagosPendientes en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "interes_pagos_pendientes")
	private BigDecimal interesPagosPendientes;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaIntPagosPendientes en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_int_pagos_pendientes")
	private BigDecimal ivaIntPagosPendientes;

	@Digits(integer = 10, message = "El campo noPagosFaltantes en Contratos no puede contener mas de 10 digitos.", fraction = 0)
	@Column(name = "no_pagos_faltantes")
	private Integer noPagosFaltantes;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_estimada_liq")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEstimadaLiq;

	@Digits(integer = 19, fraction = 4, message = "El campo interesTotal en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "interes_total")
	private BigDecimal interesTotal;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaInteresTotal en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_interes_total")
	private BigDecimal ivaInteresTotal;

	@Digits(integer = 19, fraction = 4, message = "El campo devengadoPorPagar en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "devengado_por_pagar")
	private BigDecimal devengadoPorPagar;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevengadoPorPagar en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_devengado_por_pagar")
	private BigDecimal ivaDevengadoPorPagar;

	@Size(message = "El campo idPaqueteGarantia en Contratos no debe ser mayor a 12 caracteres.", max = 12)
	@Column(name = "id_paquete_garantia")
	private String idPaqueteGarantia;

	@Digits(integer = 19, fraction = 4, message = "El campo valorGarantia en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "valor_garantia")
	private BigDecimal valorGarantia;

	@Digits(integer = 19, fraction = 4, message = "El campo montoAclaracion en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_aclaracion")
	private BigDecimal montoAclaracion;

	@Size(message = "El campo tipoResolucion en Contratos no debe ser mayor a 4 caracteres.", max = 4)
	@Column(name = "tipo_resolucion")
	private String tipoResolucion;

	@Digits(integer = 19, fraction = 4, message = "El campo devRevoUlt30d en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_revo_ult_30d")
	private BigDecimal devRevoUlt30d;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevRevoUlt30d en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_dev_revo_ult_30d")
	private BigDecimal ivaDevRevoUlt30d;

	@Digits(integer = 19, fraction = 4, message = "El campo devRevoUlt30dMant en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_revo_ult_30d_mant")
	private BigDecimal devRevoUlt30dMant;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevRevoUlt30dMant en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_dev_revo_ult_30d_mant")
	private BigDecimal ivaDevRevoUlt30dMant;

	@Digits(integer = 19, fraction = 4, message = "El campo devMoraRevoUlt30d en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_mora_revo_ult_30d")
	private BigDecimal devMoraRevoUlt30d;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevMoraRevoUlt30d en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_dev_mora_revo_ult_30d")
	private BigDecimal ivaDevMoraRevoUlt30d;

	@Digits(integer = 19, fraction = 4, message = "El campo devMoraRevoUlt30dMant en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "dev_mora_revo_ult_30d_mant")
	private BigDecimal devMoraRevoUlt30dMant;

	@Digits(integer = 19, fraction = 4, message = "El campo ivaDevMoraRevoUlt30dMant en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "iva_dev_mora_revo_ult_30d_mant")
	private BigDecimal ivaDevMoraRevoUlt30dMant;

	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Column(name = "fecha_garantiza")
	@Temporal(TemporalType.DATE)
	private Date fechaGarantiza;

	@NotNull(message = "El campo montoComisionApertura en Contratos no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo montoComisionApertura en Contratos no puede ser mayor a 19 enteros y 4 decimales.")
	@Column(name = "monto_comision_apertura")
	private BigDecimal montoComisionApertura;

	@NotNull(message = "El campo devolucionComisionApertura en Contratos no puede ser nulo.")
	@Column(name = "devolucion_comision_apertura")
	private Character devolucionComisionApertura;

	@Column(name = "fecha_esp_liq_orig")
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaEspLiqOrig;

	@Digits(integer = 3, message = "El campo noPagosOrig en Contratos no puede contener mas de 3 digitos.", fraction = 0)
	@Column(name = "no_pagos_orig")
	private Short noPagosOrig;
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contrato != null ? contrato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contratos)) {
            return false;
        }
        Contratos other = (Contratos) object;
        if ((this.contrato == null && other.contrato != null) || (this.contrato != null && !this.contrato.equals(other.contrato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mx.finsol.contratos.model.entity.Contratos[ contrato=" + contrato + " ]";
    }

}
