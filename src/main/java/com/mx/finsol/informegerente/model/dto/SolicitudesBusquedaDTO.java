package com.mx.finsol.informegerente.model.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SolicitudesBusquedaDTO {

	
	private String contratoAnt;
	private String estatus;
	private Integer idEstatus;
	private String vendedor;
	private List<String> solicitudes;
	
}
