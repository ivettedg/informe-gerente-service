package com.mx.finsol.informegerente.model.entity.burocredito;

public class OUTEmpleoBean {

	
	private String etiquetaEncabezadoSegmento;
	private String nombreRazonSocialEmpleador;
	private String primeraLineaDireccion;
	private String segundaLineaDireccion;
	private String coloniaPoblacion;
	private String delegacionMunicipio;
	private String ciudad;
	private String estado;
	private String codigoPostal;
	private String numeroTelefono;
	private String extensionTelefonica;
	private String numeroFax;
	private String cargoOcupacion;
	private String fechaContratacion;
	private String claveMonedaPagoSueldo;
	private String montoSueldoSalario;
	private String periodoPago;
	private String numeroEmpleado;
	private String fechaUltimoDiaEmpleo;
	private String fechaeporteEmpleo;
	private String fechaVerificacionEmpleo;
	private String modoVerificacion;
	private String origenRazonSocialDomicilio;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getNombreRazonSocialEmpleador() {
		return nombreRazonSocialEmpleador;
	}

	public void setNombreRazonSocialEmpleador(String nombreRazonSocialEmpleador) {
		this.nombreRazonSocialEmpleador = nombreRazonSocialEmpleador;
	}

	public String getPrimeraLineaDireccion() {
		return primeraLineaDireccion;
	}

	public void setPrimeraLineaDireccion(String primeraLineaDireccion) {
		this.primeraLineaDireccion = primeraLineaDireccion;
	}

	public String getSegundaLineaDireccion() {
		return segundaLineaDireccion;
	}

	public void setSegundaLineaDireccion(String segundaLineaDireccion) {
		this.segundaLineaDireccion = segundaLineaDireccion;
	}

	public String getColoniaPoblacion() {
		return coloniaPoblacion;
	}

	public void setColoniaPoblacion(String coloniaPoblacion) {
		this.coloniaPoblacion = coloniaPoblacion;
	}

	public String getDelegacionMunicipio() {
		return delegacionMunicipio;
	}

	public void setDelegacionMunicipio(String delegacionMunicipio) {
		this.delegacionMunicipio = delegacionMunicipio;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getExtensionTelefonica() {
		return extensionTelefonica;
	}

	public void setExtensionTelefonica(String extensionTelefonica) {
		this.extensionTelefonica = extensionTelefonica;
	}

	public String getNumeroFax() {
		return numeroFax;
	}

	public void setNumeroFax(String numeroFax) {
		this.numeroFax = numeroFax;
	}

	public String getCargoOcupacion() {
		return cargoOcupacion;
	}

	public void setCargoOcupacion(String cargoOcupacion) {
		this.cargoOcupacion = cargoOcupacion;
	}

	public String getFechaContratacion() {
		return fechaContratacion;
	}

	public void setFechaContratacion(String fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	public String getClaveMonedaPagoSueldo() {
		return claveMonedaPagoSueldo;
	}

	public void setClaveMonedaPagoSueldo(String claveMonedaPagoSueldo) {
		this.claveMonedaPagoSueldo = claveMonedaPagoSueldo;
	}

	public String getMontoSueldoSalario() {
		return montoSueldoSalario;
	}

	public void setMontoSueldoSalario(String montoSueldoSalario) {
		this.montoSueldoSalario = montoSueldoSalario;
	}

	public String getPeriodoPago() {
		return periodoPago;
	}

	public void setPeriodoPago(String periodoPago) {
		this.periodoPago = periodoPago;
	}

	public String getNumeroEmpleado() {
		return numeroEmpleado;
	}

	public void setNumeroEmpleado(String numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}

	public String getFechaUltimoDiaEmpleo() {
		return fechaUltimoDiaEmpleo;
	}

	public void setFechaUltimoDiaEmpleo(String fechaUltimoDiaEmpleo) {
		this.fechaUltimoDiaEmpleo = fechaUltimoDiaEmpleo;
	}

	public String getFechaeporteEmpleo() {
		return fechaeporteEmpleo;
	}

	public void setFechaeporteEmpleo(String fechaeporteEmpleo) {
		this.fechaeporteEmpleo = fechaeporteEmpleo;
	}

	public String getFechaVerificacionEmpleo() {
		return fechaVerificacionEmpleo;
	}

	public void setFechaVerificacionEmpleo(String fechaVerificacionEmpleo) {
		this.fechaVerificacionEmpleo = fechaVerificacionEmpleo;
	}

	public String getModoVerificacion() {
		return modoVerificacion;
	}

	public void setModoVerificacion(String modoVerificacion) {
		this.modoVerificacion = modoVerificacion;
	}

	public String getOrigenRazonSocialDomicilio() {
		return origenRazonSocialDomicilio;
	}

	public void setOrigenRazonSocialDomicilio(String origenRazonSocialDomicilio) {
		this.origenRazonSocialDomicilio = origenRazonSocialDomicilio;
	}

	@Override
	public String toString() {
		return "OUTEmpleoBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento
				+ ", nombreRazonSocialEmpleador=" + nombreRazonSocialEmpleador + ", primeraLineaDireccion="
				+ primeraLineaDireccion + ", segundaLineaDireccion=" + segundaLineaDireccion + ", coloniaPoblacion="
				+ coloniaPoblacion + ", delegacionMunicipio=" + delegacionMunicipio + ", ciudad=" + ciudad + ", estado="
				+ estado + ", codigoPostal=" + codigoPostal + ", numeroTelefono=" + numeroTelefono
				+ ", extensionTelefonica=" + extensionTelefonica + ", numeroFax=" + numeroFax + ", cargoOcupacion="
				+ cargoOcupacion + ", fechaContratacion=" + fechaContratacion + ", claveMonedaPagoSueldo="
				+ claveMonedaPagoSueldo + ", montoSueldoSalario=" + montoSueldoSalario + ", periodoPago=" + periodoPago
				+ ", numeroEmpleado=" + numeroEmpleado + ", fechaUltimoDiaEmpleo=" + fechaUltimoDiaEmpleo
				+ ", fechaeporteEmpleo=" + fechaeporteEmpleo + ", fechaVerificacionEmpleo=" + fechaVerificacionEmpleo
				+ ", modoVerificacion=" + modoVerificacion + ", origenRazonSocialDomicilio="
				+ origenRazonSocialDomicilio + "]";
	}
}
