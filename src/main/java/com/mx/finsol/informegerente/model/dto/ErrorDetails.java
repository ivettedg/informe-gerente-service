package com.mx.finsol.informegerente.model.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorDetails {

	private String idError;
	private String message;
	private String level;
	private String details;
	private Date timestamp;
	
	public ErrorDetails() {
		super();
	}

	public ErrorDetails(String idError, String message, String level, String details, Date timestamp) {
		super();
		
		this.timestamp = (Date) timestamp.clone();
		this.message = message;
		this.details = details;
		this.idError = idError;
		this.level = level;
	}

	@Override
	public String toString() {
		return "ErrorDetails [idError=" + idError + ", message=" + message + ", level=" + level + ", details=" + details
				+ ", timestamp=" + timestamp + "]";
	}
}
