package com.mx.finsol.informegerente.model.entity.burocredito;

public class OUTHistoricoSaldoBean {

	
	private String etiquetaEncabezadoSegmento;
	private String fechaReporte;
	private String montoAPagar;
	private String fechaUltimoPago;
	private String fechaUltimaCompra;
	private String creditoMaximo;
	private String saldoActual;
	private String limiteCredito;
	private String saldoVencido;
	private String montoMaximoMorosidad;
	private String fechaMaximaMorosidad;
	private String mopMaximaMorosidad;
	private String saldoInsoluto;
	private String montoUltimoPago;
	private String ultimaVezSaldoEnCero;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getFechaReporte() {
		return fechaReporte;
	}

	public void setFechaReporte(String fechaReporte) {
		this.fechaReporte = fechaReporte;
	}

	public String getMontoAPagar() {
		return montoAPagar;
	}

	public void setMontoAPagar(String montoAPagar) {
		this.montoAPagar = montoAPagar;
	}

	public String getFechaUltimoPago() {
		return fechaUltimoPago;
	}

	public void setFechaUltimoPago(String fechaUltimoPago) {
		this.fechaUltimoPago = fechaUltimoPago;
	}

	public String getFechaUltimaCompra() {
		return fechaUltimaCompra;
	}

	public void setFechaUltimaCompra(String fechaUltimaCompra) {
		this.fechaUltimaCompra = fechaUltimaCompra;
	}

	public String getCreditoMaximo() {
		return creditoMaximo;
	}

	public void setCreditoMaximo(String creditoMaximo) {
		this.creditoMaximo = creditoMaximo;
	}

	public String getSaldoActual() {
		return saldoActual;
	}

	public void setSaldoActual(String saldoActual) {
		this.saldoActual = saldoActual;
	}

	public String getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(String limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	public String getSaldoVencido() {
		return saldoVencido;
	}

	public void setSaldoVencido(String saldoVencido) {
		this.saldoVencido = saldoVencido;
	}

	public String getMontoMaximoMorosidad() {
		return montoMaximoMorosidad;
	}

	public void setMontoMaximoMorosidad(String montoMaximoMorosidad) {
		this.montoMaximoMorosidad = montoMaximoMorosidad;
	}

	public String getFechaMaximaMorosidad() {
		return fechaMaximaMorosidad;
	}

	public void setFechaMaximaMorosidad(String fechaMaximaMorosidad) {
		this.fechaMaximaMorosidad = fechaMaximaMorosidad;
	}

	public String getMopMaximaMorosidad() {
		return mopMaximaMorosidad;
	}

	public void setMopMaximaMorosidad(String mopMaximaMorosidad) {
		this.mopMaximaMorosidad = mopMaximaMorosidad;
	}

	public String getSaldoInsoluto() {
		return saldoInsoluto;
	}

	public void setSaldoInsoluto(String saldoInsoluto) {
		this.saldoInsoluto = saldoInsoluto;
	}

	public String getMontoUltimoPago() {
		return montoUltimoPago;
	}

	public void setMontoUltimoPago(String montoUltimoPago) {
		this.montoUltimoPago = montoUltimoPago;
	}

	public String getUltimaVezSaldoEnCero() {
		return ultimaVezSaldoEnCero;
	}

	public void setUltimaVezSaldoEnCero(String ultimaVezSaldoEnCero) {
		this.ultimaVezSaldoEnCero = ultimaVezSaldoEnCero;
	}

	@Override
	public String toString() {
		return "OUTHistoricoSaldoBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", fechaReporte="
				+ fechaReporte + ", montoAPagar=" + montoAPagar + ", fechaUltimoPago=" + fechaUltimoPago
				+ ", fechaUltimaCompra=" + fechaUltimaCompra + ", creditoMaximo=" + creditoMaximo + ", saldoActual="
				+ saldoActual + ", limiteCredito=" + limiteCredito + ", saldoVencido=" + saldoVencido
				+ ", montoMaximoMorosidad=" + montoMaximoMorosidad + ", fechaMaximaMorosidad=" + fechaMaximaMorosidad
				+ ", mopMaximaMorosidad=" + mopMaximaMorosidad + ", saldoInsoluto=" + saldoInsoluto
				+ ", montoUltimoPago=" + montoUltimoPago + ", ultimaVezSaldoEnCero=" + ultimaVezSaldoEnCero + "]";
	}

}
