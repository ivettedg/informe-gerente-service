package com.mx.finsol.informegerente.model.entity.solicitudes;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Folios implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String codigoFolio;
	private String folioInicial;
	private String folioActual;
	@JsonFormat(timezone = "America/Mexico_City")
	private Date fechaCaptura;
	@JsonFormat(timezone = "America/Mexico_City")
	private Date horaCaptura;
	private String usuarioCaptura;

}
