package com.mx.finsol.informegerente.model.entity.burocredito;

public class OUTCuentaBean {

	
	private String etiquetaEncabezadoSegmento;
	private String fechaActualizacion;
	private String registroInpugnado;
	private String claveUsuario;
	private String nombreUsuario;
	private String numeroTelefono;
	private String identificadorSociedadInformacionCrediticia;
	private String numeroCuenta;
	private String tipoResponsabilidad;
	private String tipoCuenta;
	private String tipoContrato;
	private String monedaCredito;
	private String importeAvaluo;
	private String numeroPagos;
	private String frecuenciaPagos;
	private String montoPagar;
	private String fechaAperturaCuentaCredito;
	private String fechaUltimoPago;
	private String fechaUltimaCompraDisposicion;
	private String fechaCierre;
	private String fechaReporteInformacion;
	private String modoReportar;
	private String ultimaFechaSaldoCero;
	private String garantia;
	private String creditoMaximoAutorizado;
	private String saldoActual;
	private String limiteCredito;
	private String saldoVencido;
	private String numeroPagosVencidos;
	private String clasificacionPuntualidadPago; // MOP (Manner of payment)
	private String historicoPagos;
	private String fechaMasRecienteHistoricoPagos;
	private String fechaMasAntiguaHistoricoPagos;
	private String claveObservacion;
	private String totalPagosReportados;
	private String totalPagosMop2;
	private String totalPagosMop3;
	private String totalPagosMop4;
	private String totalPagosMop5Mayor;
	private String saldoMorosidadHistoricaMasAlta;
	private String fechaMorosidadHistoricaMasAlta;
	private String clasificacionPuntualidadPagoMopMorosidadMasAlta;
	private String fechaInicioRestructura;
	private String montoUltimoPago;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(String fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public String getRegistroInpugnado() {
		return registroInpugnado;
	}

	public void setRegistroInpugnado(String registroInpugnado) {
		this.registroInpugnado = registroInpugnado;
	}

	public String getClaveUsuario() {
		return claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getIdentificadorSociedadInformacionCrediticia() {
		return identificadorSociedadInformacionCrediticia;
	}

	public void setIdentificadorSociedadInformacionCrediticia(String identificadorSociedadInformacionCrediticia) {
		this.identificadorSociedadInformacionCrediticia = identificadorSociedadInformacionCrediticia;
	}

	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getTipoResponsabilidad() {
		return tipoResponsabilidad;
	}

	public void setTipoResponsabilidad(String tipoResponsabilidad) {
		this.tipoResponsabilidad = tipoResponsabilidad;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getTipoContrato() {
		return tipoContrato;
	}

	public void setTipoContrato(String tipoContrato) {
		this.tipoContrato = tipoContrato;
	}

	public String getMonedaCredito() {
		return monedaCredito;
	}

	public void setMonedaCredito(String monedaCredito) {
		this.monedaCredito = monedaCredito;
	}

	public String getImporteAvaluo() {
		return importeAvaluo;
	}

	public void setImporteAvaluo(String importeAvaluo) {
		this.importeAvaluo = importeAvaluo;
	}

	public String getNumeroPagos() {
		return numeroPagos;
	}

	public void setNumeroPagos(String numeroPagos) {
		this.numeroPagos = numeroPagos;
	}

	public String getFrecuenciaPagos() {
		return frecuenciaPagos;
	}

	public void setFrecuenciaPagos(String frecuenciaPagos) {
		this.frecuenciaPagos = frecuenciaPagos;
	}

	public String getMontoPagar() {
		return montoPagar;
	}

	public void setMontoPagar(String montoPagar) {
		this.montoPagar = montoPagar;
	}

	public String getFechaAperturaCuentaCredito() {
		return fechaAperturaCuentaCredito;
	}

	public void setFechaAperturaCuentaCredito(String fechaAperturaCuentaCredito) {
		this.fechaAperturaCuentaCredito = fechaAperturaCuentaCredito;
	}

	public String getFechaUltimoPago() {
		return fechaUltimoPago;
	}

	public void setFechaUltimoPago(String fechaUltimoPago) {
		this.fechaUltimoPago = fechaUltimoPago;
	}

	public String getFechaUltimaCompraDisposicion() {
		return fechaUltimaCompraDisposicion;
	}

	public void setFechaUltimaCompraDisposicion(String fechaUltimaCompraDisposicion) {
		this.fechaUltimaCompraDisposicion = fechaUltimaCompraDisposicion;
	}

	public String getFechaCierre() {
		return fechaCierre;
	}

	public void setFechaCierre(String fechaCierre) {
		this.fechaCierre = fechaCierre;
	}

	public String getFechaReporteInformacion() {
		return fechaReporteInformacion;
	}

	public void setFechaReporteInformacion(String fechaReporteInformacion) {
		this.fechaReporteInformacion = fechaReporteInformacion;
	}

	public String getModoReportar() {
		return modoReportar;
	}

	public void setModoReportar(String modoReportar) {
		this.modoReportar = modoReportar;
	}

	public String getUltimaFechaSaldoCero() {
		return ultimaFechaSaldoCero;
	}

	public void setUltimaFechaSaldoCero(String ultimaFechaSaldoCero) {
		this.ultimaFechaSaldoCero = ultimaFechaSaldoCero;
	}

	public String getGarantia() {
		return garantia;
	}

	public void setGarantia(String garantia) {
		this.garantia = garantia;
	}

	public String getCreditoMaximoAutorizado() {
		return creditoMaximoAutorizado;
	}

	public void setCreditoMaximoAutorizado(String creditoMaximoAutorizado) {
		this.creditoMaximoAutorizado = creditoMaximoAutorizado;
	}

	public String getSaldoActual() {
		return saldoActual;
	}

	public void setSaldoActual(String saldoActual) {
		this.saldoActual = saldoActual;
	}

	public String getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(String limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	public String getSaldoVencido() {
		return saldoVencido;
	}

	public void setSaldoVencido(String saldoVencido) {
		this.saldoVencido = saldoVencido;
	}

	public String getNumeroPagosVencidos() {
		return numeroPagosVencidos;
	}

	public void setNumeroPagosVencidos(String numeroPagosVencidos) {
		this.numeroPagosVencidos = numeroPagosVencidos;
	}

	public String getClasificacionPuntualidadPago() {
		return clasificacionPuntualidadPago;
	}

	public void setClasificacionPuntualidadPago(String clasificacionPuntualidadPago) {
		this.clasificacionPuntualidadPago = clasificacionPuntualidadPago;
	}

	public String getHistoricoPagos() {
		return historicoPagos;
	}

	public void setHistoricoPagos(String historicoPagos) {
		this.historicoPagos = historicoPagos;
	}

	public String getFechaMasRecienteHistoricoPagos() {
		return fechaMasRecienteHistoricoPagos;
	}

	public void setFechaMasRecienteHistoricoPagos(String fechaMasRecienteHistoricoPagos) {
		this.fechaMasRecienteHistoricoPagos = fechaMasRecienteHistoricoPagos;
	}

	public String getFechaMasAntiguaHistoricoPagos() {
		return fechaMasAntiguaHistoricoPagos;
	}

	public void setFechaMasAntiguaHistoricoPagos(String fechaMasAntiguaHistoricoPagos) {
		this.fechaMasAntiguaHistoricoPagos = fechaMasAntiguaHistoricoPagos;
	}

	public String getClaveObservacion() {
		return claveObservacion;
	}

	public void setClaveObservacion(String claveObservacion) {
		this.claveObservacion = claveObservacion;
	}

	public String getTotalPagosReportados() {
		return totalPagosReportados;
	}

	public void setTotalPagosReportados(String totalPagosReportados) {
		this.totalPagosReportados = totalPagosReportados;
	}

	public String getTotalPagosMop2() {
		return totalPagosMop2;
	}

	public void setTotalPagosMop2(String totalPagosMop2) {
		this.totalPagosMop2 = totalPagosMop2;
	}

	public String getTotalPagosMop3() {
		return totalPagosMop3;
	}

	public void setTotalPagosMop3(String totalPagosMop3) {
		this.totalPagosMop3 = totalPagosMop3;
	}

	public String getTotalPagosMop4() {
		return totalPagosMop4;
	}

	public void setTotalPagosMop4(String totalPagosMop4) {
		this.totalPagosMop4 = totalPagosMop4;
	}

	public String getTotalPagosMop5Mayor() {
		return totalPagosMop5Mayor;
	}

	public void setTotalPagosMop5Mayor(String totalPagosMop5Mayor) {
		this.totalPagosMop5Mayor = totalPagosMop5Mayor;
	}

	public String getSaldoMorosidadHistoricaMasAlta() {
		return saldoMorosidadHistoricaMasAlta;
	}

	public void setSaldoMorosidadHistoricaMasAlta(String saldoMorosidadHistoricaMasAlta) {
		this.saldoMorosidadHistoricaMasAlta = saldoMorosidadHistoricaMasAlta;
	}

	public String getFechaMorosidadHistoricaMasAlta() {
		return fechaMorosidadHistoricaMasAlta;
	}

	public void setFechaMorosidadHistoricaMasAlta(String fechaMorosidadHistoricaMasAlta) {
		this.fechaMorosidadHistoricaMasAlta = fechaMorosidadHistoricaMasAlta;
	}

	public String getClasificacionPuntualidadPagoMopMorosidadMasAlta() {
		return clasificacionPuntualidadPagoMopMorosidadMasAlta;
	}

	public void setClasificacionPuntualidadPagoMopMorosidadMasAlta(
			String clasificacionPuntualidadPagoMopMorosidadMasAlta) {
		this.clasificacionPuntualidadPagoMopMorosidadMasAlta = clasificacionPuntualidadPagoMopMorosidadMasAlta;
	}

	public String getFechaInicioRestructura() {
		return fechaInicioRestructura;
	}

	public void setFechaInicioRestructura(String fechaInicioRestructura) {
		this.fechaInicioRestructura = fechaInicioRestructura;
	}

	public String getMontoUltimoPago() {
		return montoUltimoPago;
	}

	public void setMontoUltimoPago(String montoUltimoPago) {
		this.montoUltimoPago = montoUltimoPago;
	}

	@Override
	public String toString() {
		return "OUTCuentaBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", fechaActualizacion="
				+ fechaActualizacion + ", registroInpugnado=" + registroInpugnado + ", claveUsuario=" + claveUsuario
				+ ", nombreUsuario=" + nombreUsuario + ", numeroTelefono=" + numeroTelefono
				+ ", identificadorSociedadInformacionCrediticia=" + identificadorSociedadInformacionCrediticia
				+ ", numeroCuenta=" + numeroCuenta + ", tipoResponsabilidad=" + tipoResponsabilidad + ", tipoCuenta="
				+ tipoCuenta + ", tipoContrato=" + tipoContrato + ", monedaCredito=" + monedaCredito
				+ ", importeAvaluo=" + importeAvaluo + ", numeroPagos=" + numeroPagos + ", frecuenciaPagos="
				+ frecuenciaPagos + ", montoPagar=" + montoPagar + ", fechaAperturaCuentaCredito="
				+ fechaAperturaCuentaCredito + ", fechaUltimoPago=" + fechaUltimoPago
				+ ", fechaUltimaCompraDisposicion=" + fechaUltimaCompraDisposicion + ", fechaCierre=" + fechaCierre
				+ ", fechaReporteInformacion=" + fechaReporteInformacion + ", modoReportar=" + modoReportar
				+ ", ultimaFechaSaldoCero=" + ultimaFechaSaldoCero + ", garantia=" + garantia
				+ ", creditoMaximoAutorizado=" + creditoMaximoAutorizado + ", saldoActual=" + saldoActual
				+ ", limiteCredito=" + limiteCredito + ", saldoVencido=" + saldoVencido + ", numeroPagosVencidos="
				+ numeroPagosVencidos + ", clasificacionPuntualidadPago=" + clasificacionPuntualidadPago
				+ ", historicoPagos=" + historicoPagos + ", fechaMasRecienteHistoricoPagos="
				+ fechaMasRecienteHistoricoPagos + ", fechaMasAntiguaHistoricoPagos=" + fechaMasAntiguaHistoricoPagos
				+ ", claveObservacion=" + claveObservacion + ", totalPagosReportados=" + totalPagosReportados
				+ ", totalPagosMop2=" + totalPagosMop2 + ", totalPagosMop3=" + totalPagosMop3 + ", totalPagosMop4="
				+ totalPagosMop4 + ", totalPagosMop5Mayor=" + totalPagosMop5Mayor + ", saldoMorosidadHistoricaMasAlta="
				+ saldoMorosidadHistoricaMasAlta + ", fechaMorosidadHistoricaMasAlta=" + fechaMorosidadHistoricaMasAlta
				+ ", clasificacionPuntualidadPagoMopMorosidadMasAlta=" + clasificacionPuntualidadPagoMopMorosidadMasAlta
				+ ", fechaInicioRestructura=" + fechaInicioRestructura + ", montoUltimoPago=" + montoUltimoPago + "]";
	}
}
