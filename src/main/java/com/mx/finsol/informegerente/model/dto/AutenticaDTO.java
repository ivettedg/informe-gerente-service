package com.mx.finsol.informegerente.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AutenticaDTO {

	private String tarjetaCredito;
	private String ultimosCuatroDigitos;
	private String ejercidoCreditoHipotecario;
	private String ejercidoCreditoAutomotriz;
	
}
