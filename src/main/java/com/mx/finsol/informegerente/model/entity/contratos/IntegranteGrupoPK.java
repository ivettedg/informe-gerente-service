package com.mx.finsol.informegerente.model.entity.contratos;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class IntegranteGrupoPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull(message = "El campo contrato en IntegranteGrupo no puede ser nulo.")
	@Digits(integer = 8, fraction = 0, message = "El campo contrato en IntegranteGrupo no puede contener mas de 8 digitos.")
	@Column(name = "contrato")
	private String contrato;
	@NotNull(message = "El campo integrante en IntegranteGrupo no puede ser nulo.")
	@Digits(integer = 9, fraction = 0, message = "El campo integrante en IntegranteGrupo no puede contener mas de 9 digitos.")
	@Column(name = "integrante", nullable = false, length = 9)
	private String integrante;

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (contrato != null ? contrato.hashCode() : 0);
		hash += (integrante != null ? integrante.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof IntegranteGrupoPK)) {
			return false;
		}
		IntegranteGrupoPK other = (IntegranteGrupoPK) object;
		if ((this.contrato == null && other.contrato != null)
				|| (this.contrato != null && !this.contrato.equals(other.contrato))) {
			return false;
		}
		if ((this.integrante == null && other.integrante != null)
				|| (this.integrante != null && !this.integrante.equals(other.integrante))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.mx.finsol.informegerente.model.entity.contratos.IntegranteGrupoPK[ contrato=" + contrato + ", integrante="
				+ integrante + " ]";
	}
	
	
}
