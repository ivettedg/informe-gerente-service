package com.mx.finsol.informegerente.model.entity.solicitudes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SolicitudIntegrantes implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected SolicitudIntegrantesPK solicitudIntegrantesPK;
	private BigDecimal montoAhorro;
	private BigDecimal efectivoSolicitado;
	private BigDecimal montoSolicitado;
	private BigDecimal montoFinanciadoContAnt;
	private BigDecimal montoPago;
	private BigDecimal efectivoOtorgado;
	private BigDecimal montoOtorgado;
	private BigDecimal ingresos;
	private BigDecimal otrosIngresos;
	private BigDecimal egresos;
	private String medioContacto;
	private Integer findepScore;
	private Integer tablaScore;
	private Integer bcScore;
	private String codigoDestinoCredito;
	private String autorizoConsultaImss;
	private String claveImss;
	private String claveResoInfonavit;
	private String claveResoImss;
	private String chequePara;
	private BigDecimal montoSeguro;
	private String tipoDisposicion;
	private String cargoMesaDirectiva;
	private String contratoAnterior;
	private BigDecimal montoContratoAnterior;
	private Character hitBc;
	private Integer numeroCuentasBc;
	private String clasificacionFira;
	private Double pctParticipacion;
	private String etapaAnterior;
	private String motivoStatus;
	private String status;
	private String etapa;
	private String aprobado;
	private BigDecimal saldoActual;
	private Integer ciclo;
	private BigDecimal desertorGarantiaAplicar;
	private BigDecimal desertorDiferenciaPagar;
	private String usuarioAlta;
	private String usuarioUltMod;
	@JsonFormat(timezone = "America/Mexico_City")
	private Date fechaAlta;
	@JsonFormat(timezone = "America/Mexico_City")
	private Date fechaUltMod;
	private String cliente;
	private String referencia;
	private String seguro;
	private String campaniaIguala;
	private String plazoSeguro;
	private String seguroEnfermedad;
	private String desertor;
	private String perCreRenovar;
	private String empresaOrigen;
	private String origenElimina;
	private String usuarioElimina;
	private String folioConsultaBuro;
	private Integer idStatus;
	private SolicitudesFinsol solicitudesFinsol;

}
