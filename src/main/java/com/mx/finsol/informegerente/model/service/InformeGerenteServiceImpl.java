package com.mx.finsol.informegerente.model.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import com.mx.finsol.informegerente.client.InformeBuroWSClient;
import com.mx.finsol.informegerente.exception.CustomException;
import com.mx.finsol.informegerente.model.dto.BuroBusquedaDTO;
import com.mx.finsol.informegerente.model.dto.SolicitudesBusquedaDTO;
import com.mx.finsol.informegerente.model.entity.burocredito.OUTCuentaBean;
import com.mx.finsol.informegerente.model.entity.burocredito.OUTNombreBean;
import com.mx.finsol.informegerente.model.entity.burocredito.OUTPersonaBuroBean;
import com.mx.finsol.informegerente.model.entity.burocredito.OUTResumenBean;
import com.mx.finsol.informegerente.model.entity.solicitudes.CatEstatus;
import com.mx.finsol.informegerente.model.entity.solicitudes.SolicitudIntegrantes;
import com.mx.finsol.informegerente.model.entity.solicitudes.SolicitudesFinsol;

@Service
public class InformeGerenteServiceImpl implements InformeGerenteService{

}
