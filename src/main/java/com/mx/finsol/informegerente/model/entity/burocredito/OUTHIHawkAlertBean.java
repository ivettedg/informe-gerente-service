package com.mx.finsol.informegerente.model.entity.burocredito;

public class OUTHIHawkAlertBean {

	
	private String etiquetaEncabezadoSegmento;
	private String fechaReporte;
	private String codigoPrevencion;
	private String tipoUsuario;
	private String mensaje;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getFechaReporte() {
		return fechaReporte;
	}

	public void setFechaReporte(String fechaReporte) {
		this.fechaReporte = fechaReporte;
	}

	public String getCodigoPrevencion() {
		return codigoPrevencion;
	}

	public void setCodigoPrevencion(String codigoPrevencion) {
		this.codigoPrevencion = codigoPrevencion;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@Override
	public String toString() {
		return "OUTHIHawkAlertBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", fechaReporte="
				+ fechaReporte + ", codigoPrevencion=" + codigoPrevencion + ", tipoUsuario=" + tipoUsuario
				+ ", mensaje=" + mensaje + "]";
	}

}
