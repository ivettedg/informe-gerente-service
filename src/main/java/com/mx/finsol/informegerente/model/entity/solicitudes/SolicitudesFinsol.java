package com.mx.finsol.informegerente.model.entity.solicitudes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SolicitudesFinsol implements Serializable{

	/**
	 * 
	 */
		private static final long serialVersionUID = 1L;
	
	 	private String solicitud;
	    private String claveEmpresa;
	    private String creditoGrupal;
	    private Short numeroIntegrantes;
	    private Integer oficina;
	    private String segmento;
	    private String categoria;
	    private String codigoProducto;
	    private String cliente;
	    private BigDecimal efectivoSolicitado;
	    private BigDecimal montoSolicitado;
	    private String frecuenciaPago;
	    private Integer plazoSolicitado;
	    private Short diaPago;
	    private Double tasa;
	    private BigDecimal comisionApertura;
	    private Double cat;
	    private Short oficinaDisposicion;
	    private Integer plazo;
	    private String horaReunion;
	    private Short diaReunion;
	    private Integer numeroFamiliaresGrupo;
	    private BigDecimal montoPago;
	    private BigDecimal montoFinanciadoContAnt;
	    private String contratoAnt;
		@JsonFormat(timezone = "America/Mexico_City")
	    private Date fechaResolucion;
		@JsonFormat(timezone = "America/Mexico_City")
	    private Date horaResolucion;
	    private BigDecimal efectivoOtorgado;
	    private BigDecimal montoOtorgado;
	    private String contrato;
	    private String vendedor;
	    private String vendedorOrig;
	    private String gteVentas;
	    private Integer grupoVentas;
	    private String gteSucursal;
	    private String contratoRecomienda;
	    private String claveCorresponsal;
	    private String status;
	    private Integer ciclo;
	    private String origenVenta;
	    private String etapa;
	    private String tipoVerificacion;
	    @JsonFormat(timezone = "America/Mexico_City")
	    private Date fechaCaptura;
	    @JsonFormat(timezone = "America/Mexico_City")
	    private Date horaCaptura;
	    private String etapaAnterior;
	    private String motivoStatus;
	    @JsonFormat(timezone = "America/Mexico_City")
	    private Date fechaUltMod;
	    @JsonFormat(timezone = "America/Mexico_City")
	    private Date horaUltMod;
	    private String horaReunionFin;
	    private String personaDomReunion;
	    private String express;
	    private String usuarioRegistraSolicitud;
	    private String usuarioUltMod;
	    private String excepcionGarantia;
	    private String autorizacionGarantia;
	    private String usuarioAutorizacion;
	    @JsonFormat(timezone = "America/Mexico_City")
	    private Date fechaAutorizacion;
	    private Character seguro;
	    private String estatus;
	    private String descripcion;
	    @JsonFormat(timezone = "America/Mexico_City")
	    private Date fechaFin;
	    private String campana;
	    private String senalamientoExcepcion;
	    private String asignacionCec;
	    @JsonFormat(timezone = "America/Mexico_City")
	    private Date dtAsignacionCec;
	    private String traspasoGarantia;
	    private BigDecimal pctGarantia;
	    private String motivoCancelacion;
	    private String aplicarGarantia;
	    private String aplicarGarantiaDesertor;
	    private String garantiaJustificacion;
	    private BigDecimal competenciaMontoCapital;
	    private BigDecimal competenciaTotalPagar;
	    private Integer competenciaPlazo;
	    private Integer competenciaTotalIntegrantes;
	    private Double competenciaTasaAnual;
	    @JsonFormat(timezone = "America/Mexico_City")
	    private Date fechaAprobacionCec;
	    @JsonFormat(timezone = "America/Mexico_City")
	    private Date fechaRegExpCm;
	    @JsonFormat(timezone = "America/Mexico_City")
	    private Date fechaAsignaDocumental;
	    private Integer reactivacion;
	    private String solicitudOrigen;
	    private Integer idEstatus;
	    private Integer idStatus;
	    private List<SolicitudIntegrantes> solicitudIntegrantesList;
	    private BitacoraAtomosSolicitudes bitacoraAtomosSolicitudes;

}
