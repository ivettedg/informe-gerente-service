package com.mx.finsol.informegerente.model.entity.burocredito;

import java.util.List;

public class OUTPersonaBuroBean {

	
	private OUTEncabezadoBean encabezado;
	private OUTNombreBean nombre;
	private List<OUTDireccionBean> domicilios;
	private List<OUTEmpleoBean> empleos;
	private List<OUTCuentaBean> cuentas;
	private List<OUTHistoricoSaldoBean> saldos;
	private List<OUTConsultaBean> consultas;
	private List<OUTResumenBean> resumenInformeBuro;
	private List<OUTHIHawkAlertBean> alertasHawkHI;
	private List<OUTHRHawkAlertBean> alertasHawkHR;
	private OUTDeclarativaBean declarativa;
	private List<OutScoreBean> scoreBuroCredito;
	private OUTCierreBean cierre;

	public OUTEncabezadoBean getEncabezado() {
		return encabezado;
	}

	public void setEncabezado(OUTEncabezadoBean encabezado) {
		this.encabezado = encabezado;
	}

	public OUTNombreBean getNombre() {
		return nombre;
	}

	public void setNombre(OUTNombreBean nombre) {
		this.nombre = nombre;
	}

	public List<OUTDireccionBean> getDomicilios() {
		return domicilios;
	}

	public void setDomicilios(List<OUTDireccionBean> domicilios) {
		this.domicilios = domicilios;
	}

	public List<OUTEmpleoBean> getEmpleos() {
		return empleos;
	}

	public void setEmpleos(List<OUTEmpleoBean> empleos) {
		this.empleos = empleos;
	}

	public List<OUTCuentaBean> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<OUTCuentaBean> cuentas) {
		this.cuentas = cuentas;
	}

	public List<OUTHistoricoSaldoBean> getSaldos() {
		return saldos;
	}

	public void setSaldos(List<OUTHistoricoSaldoBean> saldos) {
		this.saldos = saldos;
	}

	public List<OUTConsultaBean> getConsultas() {
		return consultas;
	}

	public void setConsultas(List<OUTConsultaBean> consultas) {
		this.consultas = consultas;
	}

	public List<OUTResumenBean> getResumenInformeBuro() {
		return resumenInformeBuro;
	}

	public void setResumenInformeBuro(List<OUTResumenBean> resumenInformeBuro) {
		this.resumenInformeBuro = resumenInformeBuro;
	}

	public List<OUTHIHawkAlertBean> getAlertasHawkHI() {
		return alertasHawkHI;
	}

	public void setAlertasHawkHI(List<OUTHIHawkAlertBean> alertasHawkHI) {
		this.alertasHawkHI = alertasHawkHI;
	}

	public List<OUTHRHawkAlertBean> getAlertasHawkHR() {
		return alertasHawkHR;
	}

	public void setAlertasHawkHR(List<OUTHRHawkAlertBean> alertasHawkHR) {
		this.alertasHawkHR = alertasHawkHR;
	}

	public OUTDeclarativaBean getDeclarativa() {
		return declarativa;
	}

	public void setDeclarativa(OUTDeclarativaBean declarativa) {
		this.declarativa = declarativa;
	}

	public List<OutScoreBean> getScoreBuroCredito() {
		return scoreBuroCredito;
	}

	public void setScoreBuroCredito(List<OutScoreBean> scoreBuroCredito) {
		this.scoreBuroCredito = scoreBuroCredito;
	}

	public OUTCierreBean getCierre() {
		return cierre;
	}

	public void setCierre(OUTCierreBean cierre) {
		this.cierre = cierre;
	}

	@Override
	public String toString() {
		return "OUTPersonaBuroBean [encabezado=" + encabezado + ", nombre=" + nombre + ", domicilios=" + domicilios
				+ ", empleos=" + empleos + ", cuentas=" + cuentas + ", saldos=" + saldos + ", consultas=" + consultas
				+ ", resumenInformeBuro=" + resumenInformeBuro + ", alertasHawkHI=" + alertasHawkHI + ", alertasHawkHR="
				+ alertasHawkHR + ", declarativa=" + declarativa + ", scoreBuroCredito=" + scoreBuroCredito
				+ ", cierre=" + cierre + "]";
	}
}
