package com.mx.finsol.informegerente.model.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SolicitudesBusquedaIntegrantesDTO {

	private List<String> clientes;
	private Integer idStatus;
	private List<String> solicitudes;
	private String status;
	
	
}
