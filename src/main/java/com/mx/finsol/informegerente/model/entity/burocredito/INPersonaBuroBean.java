package com.mx.finsol.informegerente.model.entity.burocredito;

import java.util.List;

public class INPersonaBuroBean {

	private INEncabezadoBean encabezado;
	private INNombreBean nombre;
	private List<INDomicilioBean> domicilios;
	private List<INEmpleoBean> empleos;
	private List<INCuentaBean> cuentas;
	private INAutenticaBean autentica;
	private INCierreBean cierre;

	public INEncabezadoBean getEncabezado() {
		return encabezado;
	}

	public void setEncabezado(INEncabezadoBean encabezado) {
		this.encabezado = encabezado;
	}

	public INNombreBean getNombre() {
		return nombre;
	}

	public void setNombre(INNombreBean nombre) {
		this.nombre = nombre;
	}

	public List<INDomicilioBean> getDomicilios() {
		return domicilios;
	}

	public void setDomicilios(List<INDomicilioBean> domicilios) {
		this.domicilios = domicilios;
	}

	public List<INEmpleoBean> getEmpleos() {
		return empleos;
	}

	public void setEmpleos(List<INEmpleoBean> empleos) {
		this.empleos = empleos;
	}

	public List<INCuentaBean> getCuentas() {
		return cuentas;
	}

	public void setCuentas(List<INCuentaBean> cuentas) {
		this.cuentas = cuentas;
	}

	public INCierreBean getCierre() {
		return cierre;
	}

	public void setCierre(INCierreBean cierre) {
		this.cierre = cierre;
	}

	public INAutenticaBean getAutentica() {
		return autentica;
	}

	public void setAutentica(INAutenticaBean autentica) {
		this.autentica = autentica;
	}

}
