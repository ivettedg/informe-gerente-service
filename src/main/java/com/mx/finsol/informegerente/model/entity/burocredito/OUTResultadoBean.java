package com.mx.finsol.informegerente.model.entity.burocredito;

import java.util.List;

public class OUTResultadoBean {

	
	private String claveEmpresa;
	private String usuarioConsulta;
	private String transaccionId;
	private String origen;

	private List<OUTPersonaBuroBean> personas;

	public String getClaveEmpresa() {
		return claveEmpresa;
	}

	public void setClaveEmpresa(String claveEmpresa) {
		this.claveEmpresa = claveEmpresa;
	}

	public String getUsuarioConsulta() {
		return usuarioConsulta;
	}

	public void setUsuarioConsulta(String usuarioConsulta) {
		this.usuarioConsulta = usuarioConsulta;
	}

	public String getTransaccionId() {
		return transaccionId;
	}

	public void setTransaccionId(String transaccionId) {
		this.transaccionId = transaccionId;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public List<OUTPersonaBuroBean> getPersonas() {
		return personas;
	}

	public void setPersonas(List<OUTPersonaBuroBean> personas) {
		this.personas = personas;
	}

	@Override
	public String toString() {
		return "OUTResultadoBean [claveEmpresa=" + claveEmpresa + ", usuarioConsulta=" + usuarioConsulta
				+ ", transaccionId=" + transaccionId + ", origen=" + origen + ", personas=" + personas + "]";
	}

}
