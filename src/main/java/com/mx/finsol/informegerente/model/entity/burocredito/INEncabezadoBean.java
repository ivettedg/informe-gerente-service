package com.mx.finsol.informegerente.model.entity.burocredito;

public class INEncabezadoBean {

	
	private String etiquetaSegmento;
	private String version;
	private String numeroReferenciaOperador;
	private String productoRequerido;
	private String clavePais;
	private String reservado;
	private String claveUsuario;
	private String passwordAcceso;
	private String tipoResponsabilidad;
	private String tipoContratoProducto;
	private String monedaCredito;
	private String importeContrato;
	private String idioma;
	private String tipoSalida;
	private String tamanioBloqueRegistroRespuesta;
	private String identificacionImpresora;
	private String reservadoUsoFuturo;

	public String getEtiquetaSegmento() {
		return etiquetaSegmento;
	}

	public void setEtiquetaSegmento(String etiquetaSegmento) {
		this.etiquetaSegmento = etiquetaSegmento;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getNumeroReferenciaOperador() {
		return numeroReferenciaOperador;
	}

	public void setNumeroReferenciaOperador(String numeroReferenciaOperador) {
		this.numeroReferenciaOperador = numeroReferenciaOperador;
	}

	public String getProductoRequerido() {
		return productoRequerido;
	}

	public void setProductoRequerido(String productoRequerido) {
		this.productoRequerido = productoRequerido;
	}

	public String getClavePais() {
		return clavePais;
	}

	public void setClavePais(String clavePais) {
		this.clavePais = clavePais;
	}

	public String getReservado() {
		return reservado;
	}

	public void setReservado(String reservado) {
		this.reservado = reservado;
	}

	public String getClaveUsuario() {
		return claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}

	public String getPasswordAcceso() {
		return passwordAcceso;
	}

	public void setPasswordAcceso(String passwordAcceso) {
		this.passwordAcceso = passwordAcceso;
	}

	public String getTipoResponsabilidad() {
		return tipoResponsabilidad;
	}

	public void setTipoResponsabilidad(String tipoResponsabilidad) {
		this.tipoResponsabilidad = tipoResponsabilidad;
	}

	public String getTipoContratoProducto() {
		return tipoContratoProducto;
	}

	public void setTipoContratoProducto(String tipoContratoProducto) {
		this.tipoContratoProducto = tipoContratoProducto;
	}

	public String getMonedaCredito() {
		return monedaCredito;
	}

	public void setMonedaCredito(String monedaCredito) {
		this.monedaCredito = monedaCredito;
	}

	public String getImporteContrato() {
		return importeContrato;
	}

	public void setImporteContrato(String importeContrato) {
		this.importeContrato = importeContrato;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getTipoSalida() {
		return tipoSalida;
	}

	public void setTipoSalida(String tipoSalida) {
		this.tipoSalida = tipoSalida;
	}

	public String getTamanioBloqueRegistroRespuesta() {
		return tamanioBloqueRegistroRespuesta;
	}

	public void setTamanioBloqueRegistroRespuesta(String tamanioBloqueRegistroRespuesta) {
		this.tamanioBloqueRegistroRespuesta = tamanioBloqueRegistroRespuesta;
	}

	public String getIdentificacionImpresora() {
		return identificacionImpresora;
	}

	public void setIdentificacionImpresora(String identificacionImpresora) {
		this.identificacionImpresora = identificacionImpresora;
	}

	public String getReservadoUsoFuturo() {
		return reservadoUsoFuturo;
	}

	public void setReservadoUsoFuturo(String reservadoUsoFuturo) {
		this.reservadoUsoFuturo = reservadoUsoFuturo;
	}

	@Override
	public String toString() {
		return "INEncabezadoBean [etiquetaSegmento=" + etiquetaSegmento + ", version=" + version
				+ ", numeroReferenciaOperador=" + numeroReferenciaOperador + ", productoRequerido=" + productoRequerido
				+ ", clavePais=" + clavePais + ", reservado=" + reservado + ", claveUsuario=" + claveUsuario
				+ ", passwordAcceso=" + passwordAcceso + ", tipoResponsabilidad=" + tipoResponsabilidad
				+ ", tipoContratoProducto=" + tipoContratoProducto + ", monedaCredito=" + monedaCredito
				+ ", importeContrato=" + importeContrato + ", idioma=" + idioma + ", tipoSalida=" + tipoSalida
				+ ", tamanioBloqueRegistroRespuesta=" + tamanioBloqueRegistroRespuesta + ", identificacionImpresora="
				+ identificacionImpresora + ", reservadoUsoFuturo=" + reservadoUsoFuturo + "]";
	}

}
