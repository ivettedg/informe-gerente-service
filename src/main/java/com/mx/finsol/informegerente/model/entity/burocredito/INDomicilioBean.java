package com.mx.finsol.informegerente.model.entity.burocredito;

public class INDomicilioBean {

	private String etiquetaEncabezadoSegmento;
	private String direccion1;
	private String direccion2;
	private String coloniaPoblacion;
	private String delegacionMunicipio;
	private String ciudad;
	private String estado;
	private String cp;
	private String fechaResidencia;
	private String numeroTelefono;
	private String extension;
	private String fax;
	private String tipoDomicilio;
	private String indicadorEspecialDomicilio;
	private String codPais;
	private String fechaReporteDireccion;
	private String origenDomicilio;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getDireccion1() {
		return direccion1;
	}

	public void setDireccion1(String direccion1) {
		this.direccion1 = direccion1;
	}

	public String getDireccion2() {
		return direccion2;
	}

	public void setDireccion2(String direccion2) {
		this.direccion2 = direccion2;
	}

	public String getColoniaPoblacion() {
		return coloniaPoblacion;
	}

	public void setColoniaPoblacion(String coloniaPoblacion) {
		this.coloniaPoblacion = coloniaPoblacion;
	}

	public String getDelegacionMunicipio() {
		return delegacionMunicipio;
	}

	public void setDelegacionMunicipio(String delegacionMunicipio) {
		this.delegacionMunicipio = delegacionMunicipio;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getFechaResidencia() {
		return fechaResidencia;
	}

	public void setFechaResidencia(String fechaResidencia) {
		this.fechaResidencia = fechaResidencia;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}

	public void setNumeroTelefono(String numeroTelefono) {
		this.numeroTelefono = numeroTelefono;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getTipoDomicilio() {
		return tipoDomicilio;
	}

	public void setTipoDomicilio(String tipoDomicilio) {
		this.tipoDomicilio = tipoDomicilio;
	}

	public String getIndicadorEspecialDomicilio() {
		return indicadorEspecialDomicilio;
	}

	public void setIndicadorEspecialDomicilio(String indicadorEspecialDomicilio) {
		this.indicadorEspecialDomicilio = indicadorEspecialDomicilio;
	}

	public String getCodPais() {
		return codPais;
	}

	public void setCodPais(String codPais) {
		this.codPais = codPais;
	}

	public String getFechaReporteDireccion() {
		return fechaReporteDireccion;
	}

	public void setFechaReporteDireccion(String fechaReporteDireccion) {
		this.fechaReporteDireccion = fechaReporteDireccion;
	}

	public String getOrigenDomicilio() {
		return origenDomicilio;
	}

	public void setOrigenDomicilio(String origenDomicilio) {
		this.origenDomicilio = origenDomicilio;
	}

	@Override
	public String toString() {
		return "INDomicilioBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", direccion1="
				+ direccion1 + ", direccion2=" + direccion2 + ", coloniaPoblacion=" + coloniaPoblacion
				+ ", delegacionMunicipio=" + delegacionMunicipio + ", ciudad=" + ciudad + ", estado=" + estado + ", cp="
				+ cp + ", fechaResidencia=" + fechaResidencia + ", numeroTelefono=" + numeroTelefono + ", extension="
				+ extension + ", fax=" + fax + ", tipoDomicilio=" + tipoDomicilio + ", indicadorEspecialDomicilio="
				+ indicadorEspecialDomicilio + ", codPais=" + codPais + ", fechaReporteDireccion="
				+ fechaReporteDireccion + ", origenDomicilio=" + origenDomicilio + "]";
	}
}
