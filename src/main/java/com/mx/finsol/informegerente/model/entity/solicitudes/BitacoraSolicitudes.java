package com.mx.finsol.informegerente.model.entity.solicitudes;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BitacoraSolicitudes implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer idBitacoraSolicitudes;
    private String solicitud;
    private String cliente;
    private Integer tipoRegistro;
    private String usuarioMod;
    @JsonFormat(timezone = "America/Mexico_City")
    private Date fechaMod;
    private Integer idStatus;

}
