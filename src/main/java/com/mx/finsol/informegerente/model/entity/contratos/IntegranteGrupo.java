package com.mx.finsol.informegerente.model.entity.contratos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "integrante_grupo", catalog = "credprod_finsol", schema = "dbo")
@XmlRootElement
public class IntegranteGrupo implements Serializable {

	private static final long serialVersionUID = 1L;
	@EmbeddedId
	protected IntegranteGrupoPK integranteGrupoPK;
	@NotNull(message = "El campo rol en IntegranteGrupo no puede ser nulo.")
	@Size(message = "El campo rol en IntegranteGrupo no puede ser mayor a 4 caracteres.", max = 4)
	@Column(name = "rol")
	private String rol;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@NotNull(message = "El campo montoOtorgado en IntegranteGrupo no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo montoOtorgado en IntegranteGrupo no puede contener mas de 19 digitos y 4 decimales.")
	@Column(name = "monto_otorgado")
	private BigDecimal montoOtorgado;
	@Column(name = "cheque")
	private Integer cheque;
	@Size(message = "El campo destinoCredito en IntegranteGrupo no puede ser mayor a 80 caracteres.", max = 80)
	@Column(name = "destino_credito")
	private String destinoCredito;
	@Digits(integer = 19, fraction = 4, message = "El campo montoLiqCtrant en IntegranteGrupo no puede contener mas de 19 digitos y 4 decimales.")
	@Column(name = "monto_liq_ctrant")
	private BigDecimal montoLiqCtrant;
	@NotNull(message = "El campo status en IntegranteGrupo no puede ser nulo.")
	@Column(name = "status")
	private Character status;
	@NotNull(message = "El campo cicloIntegrante en IntegranteGrupo no puede ser nulo.")
	@Column(name = "ciclo_integrante")
	private Integer cicloIntegrante;
	@NotNull(message = "El campo capitalInsoluto en IntegranteGrupo no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo capitalInsoluto en IntegranteGrupo no puede contener mas de 19 digitos y 4 decimales.")
	@Column(name = "capital_insoluto")
	private BigDecimal capitalInsoluto;
	@Column(name = "clasificacion_fira")
	private Character clasificacionFira;
	@Size(message = "El campo localidadEstandar en IntegranteGrupo no puede ser mayor a 10 caracteres.", max = 10)
	@Column(name = "localidad_estandar")
	private String localidadEstandar;
	@Column(name = "fecha_act_status")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaActStatus;
	@Digits(integer = 15, fraction = 0, message = "El campo pctParticipacion en IntegranteGrupo no puede contener mas de 15 digitos.")
	@Column(name = "pct_participacion")
	private Double pctParticipacion;
	@Size(message = "El campo garantiza en IntegranteGrupo no puede ser mayor a 9 caracteres.", max = 9)
	@Column(name = "garantiza")
	private String garantiza;
	@Size(message = "El campo idPaqueteGarantia en IntegranteGrupo no puede ser mayor a 12 caracteres.", max = 12)
	@Column(name = "id_paquete_garantia")
	private String idPaqueteGarantia;
	@Digits(integer = 19, fraction = 4, message = "El campo valorGarantia en IntegranteGrupo no puede contener mas de 19 digitos y 4 decimales.")
	@Column(name = "valor_garantia", precision = 19, scale = 4)
	private BigDecimal valorGarantia;
	@Digits(integer = 19, fraction = 4, message = "El campo montoAhorro en IntegranteGrupo no puede contener mas de 19 digitos y 4 decimales.")
	@Column(name = "monto_ahorro", precision = 19, scale = 4)
	private BigDecimal montoAhorro;
	@Column(name = "colinda_zona_aut")
	private Character colindaZonaAut;
	@NotNull(message = "El campo montoSeguro en IntegranteGrupo no puede ser nulo.")
	@Digits(integer = 19, fraction = 4, message = "El campo montoSeguro en IntegranteGrupo no puede contener mas de 19 digitos y 4 decimales.")
	@Column(name = "monto_seguro")
	private BigDecimal montoSeguro;
	@Column(name = "fecha_garantiza")
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Temporal(TemporalType.DATE)
	private Date fechaGarantiza;
	@NotNull(message = "El campo folioCheque en IntegranteGrupo no puede ser nulo.")
	@Column(name = "folio_cheque")
	private Long folioCheque;
	@Column(name = "status_buro")
	private Character statusBuro;
	@Column(name = "fecha_rep_buro")
	@JsonFormat(pattern = "yyyy-MM-dd" ,timezone="America/Mexico_City" )
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRepBuro;
	@Size(message = "El campo codErrBuro en IntegranteGrupo no puede ser mayor a 4 caracteres.", max = 4)
	@Column(name = "cod_err_buro")
	private String codErrBuro;
	@NotNull(message = "El campo montoSeguroVida en IntegranteGrupo no puede ser nulo.")
	@Size(message = "El campo montoSeguroVida en IntegranteGrupo no puede ser mayor a 4 caracteres.", max = 4)
	@Column(name = "monto_seguro_vida")
	private BigDecimal montoSeguroVida;
	@NotNull(message = "El campo montoSeguroEnfermedad en IntegranteGrupo no puede ser nulo.")
	@Size(message = "El campo montoSeguroEnfermedad en IntegranteGrupo no puede ser mayor a 4 caracteres.", max = 4)
	@Column(name = "monto_seguro_enfermedad")
	private BigDecimal montoSeguroEnfermedad;
	@NotNull(message = "El campo comisionAperturaIntegrante en IntegranteGrupo no puede ser nulo.")
	@Size(message = "El campo comisionAperturaIntegrante en IntegranteGrupo no puede ser mayor a 4 caracteres.", max = 4)
	@Column(name = "comision_apertura_integrante")
	private BigDecimal comisionAperturaIntegrante;
	@NotNull(message = "El campo capitalInsolutoCierre en IntegranteGrupo no puede ser nulo.")
	@Size(message = "El campo capitalInsolutoCierre en IntegranteGrupo no puede ser mayor a 4 caracteres.", max = 4)
	@Column(name = "capital_insoluto_cierre")
	private BigDecimal capitalInsolutoCierre;

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (integranteGrupoPK != null ? integranteGrupoPK.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof IntegranteGrupo)) {
			return false;
		}
		IntegranteGrupo other = (IntegranteGrupo) object;
		if ((this.integranteGrupoPK == null && other.integranteGrupoPK != null)
				|| (this.integranteGrupoPK != null && !this.integranteGrupoPK.equals(other.integranteGrupoPK))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.mx.finsol.informegerente.model.entity.contratos.IntegranteGrupo[ integranteGrupoPK=" + integranteGrupoPK + " ]";
	}

}