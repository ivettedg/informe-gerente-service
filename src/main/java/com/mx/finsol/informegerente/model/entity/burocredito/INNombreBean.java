package com.mx.finsol.informegerente.model.entity.burocredito;

public class INNombreBean {

	private String etiquetaEncabezadoSegmento;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String apellidoAdicional;
	private String primerNombre;
	private String segundoNombre;
	private String fechaNacimiento;
	private String rfc;
	private String prefijo;
	private String sufijo;
	private String nacionalidad;
	private String residencia;
	private String numeroLicenciaConducir;
	private String estadoCivil;
	private String sexo;
	private String numeroCedulaProfesional;
	private String numeroRegistroElectoral;
	private String claveImpuestosOtroPais; // Review
	private String claveOtroPais; // Review
	private String claveIdentificacionUnica;
	private String clavePais;
	private String numeroDependientes;
	private String edadesDependientes;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getApellidoAdicional() {
		return apellidoAdicional;
	}

	public void setApellidoAdicional(String apellidoAdicional) {
		this.apellidoAdicional = apellidoAdicional;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getPrefijo() {
		return prefijo;
	}

	public void setPrefijo(String prefijo) {
		this.prefijo = prefijo;
	}

	public String getSufijo() {
		return sufijo;
	}

	public void setSufijo(String sufijo) {
		this.sufijo = sufijo;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getResidencia() {
		return residencia;
	}

	public void setResidencia(String residencia) {
		this.residencia = residencia;
	}

	public String getNumeroLicenciaConducir() {
		return numeroLicenciaConducir;
	}

	public void setNumeroLicenciaConducir(String numeroLicenciaConducir) {
		this.numeroLicenciaConducir = numeroLicenciaConducir;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getNumeroCedulaProfesional() {
		return numeroCedulaProfesional;
	}

	public void setNumeroCedulaProfesional(String numeroCedulaProfesional) {
		this.numeroCedulaProfesional = numeroCedulaProfesional;
	}

	public String getNumeroRegistroElectoral() {
		return numeroRegistroElectoral;
	}

	public void setNumeroRegistroElectoral(String numeroRegistroElectoral) {
		this.numeroRegistroElectoral = numeroRegistroElectoral;
	}

	public String getClaveImpuestosOtroPais() {
		return claveImpuestosOtroPais;
	}

	public void setClaveImpuestosOtroPais(String claveImpuestosOtroPais) {
		this.claveImpuestosOtroPais = claveImpuestosOtroPais;
	}

	public String getClaveOtroPais() {
		return claveOtroPais;
	}

	public void setClaveOtroPais(String claveOtroPais) {
		this.claveOtroPais = claveOtroPais;
	}

	public String getClaveIdentificacionUnica() {
		return claveIdentificacionUnica;
	}

	public void setClaveIdentificacionUnica(String claveIdentificacionUnica) {
		this.claveIdentificacionUnica = claveIdentificacionUnica;
	}

	public String getClavePais() {
		return clavePais;
	}

	public void setClavePais(String clavePais) {
		this.clavePais = clavePais;
	}

	public String getNumeroDependientes() {
		return numeroDependientes;
	}

	public void setNumeroDependientes(String numeroDependientes) {
		this.numeroDependientes = numeroDependientes;
	}

	public String getEdadesDependientes() {
		return edadesDependientes;
	}

	public void setEdadesDependientes(String edadesDependientes) {
		this.edadesDependientes = edadesDependientes;
	}

	@Override
	public String toString() {
		return "INNombreBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", apellidoPaterno="
				+ apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno + ", apellidoAdicional=" + apellidoAdicional
				+ ", primerNombre=" + primerNombre + ", segundoNombre=" + segundoNombre + ", fechaNacimiento="
				+ fechaNacimiento + ", rfc=" + rfc + ", prefijo=" + prefijo + ", sufijo=" + sufijo + ", nacionalidad="
				+ nacionalidad + ", residencia=" + residencia + ", numeroLicenciaConducir=" + numeroLicenciaConducir
				+ ", estadoCivil=" + estadoCivil + ", sexo=" + sexo + ", numeroCedulaProfesional="
				+ numeroCedulaProfesional + ", numeroRegistroElectoral=" + numeroRegistroElectoral
				+ ", claveImpuestosOtroPais=" + claveImpuestosOtroPais + ", claveOtroPais=" + claveOtroPais
				+ ", claveIdentificacionUnica=" + claveIdentificacionUnica + ", clavePais=" + clavePais
				+ ", numeroDependientes=" + numeroDependientes + ", edadesDependientes=" + edadesDependientes + "]";
	}

}
