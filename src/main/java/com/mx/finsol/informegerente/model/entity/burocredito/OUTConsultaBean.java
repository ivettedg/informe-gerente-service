package com.mx.finsol.informegerente.model.entity.burocredito;

public class OUTConsultaBean {

	
	private String etiquetaEncabezadoSegmento;
	private String fechaConsulta;
	private String reservadoUsoFuturo;
	private String claveUsuario;
	private String nombreUsuario;
	private String numeroTelefonoUsuario;
	private String tipoContratoProducto;
	private String monedaDelCredito;
	private String importeContrato;
	private String tipoResponsabilidadCuenta;
	private String indicadorClienteNuevo;
	private String reservado;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getFechaConsulta() {
		return fechaConsulta;
	}

	public void setFechaConsulta(String fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	public String getReservadoUsoFuturo() {
		return reservadoUsoFuturo;
	}

	public void setReservadoUsoFuturo(String reservadoUsoFuturo) {
		this.reservadoUsoFuturo = reservadoUsoFuturo;
	}

	public String getClaveUsuario() {
		return claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getNumeroTelefonoUsuario() {
		return numeroTelefonoUsuario;
	}

	public void setNumeroTelefonoUsuario(String numeroTelefonoUsuario) {
		this.numeroTelefonoUsuario = numeroTelefonoUsuario;
	}

	public String getTipoContratoProducto() {
		return tipoContratoProducto;
	}

	public void setTipoContratoProducto(String tipoContratoProducto) {
		this.tipoContratoProducto = tipoContratoProducto;
	}

	public String getMonedaDelCredito() {
		return monedaDelCredito;
	}

	public void setMonedaDelCredito(String monedaDelCredito) {
		this.monedaDelCredito = monedaDelCredito;
	}

	public String getImporteContrato() {
		return importeContrato;
	}

	public void setImporteContrato(String importeContrato) {
		this.importeContrato = importeContrato;
	}

	public String getTipoResponsabilidadCuenta() {
		return tipoResponsabilidadCuenta;
	}

	public void setTipoResponsabilidadCuenta(String tipoResponsabilidadCuenta) {
		this.tipoResponsabilidadCuenta = tipoResponsabilidadCuenta;
	}

	public String getIndicadorClienteNuevo() {
		return indicadorClienteNuevo;
	}

	public void setIndicadorClienteNuevo(String indicadorClienteNuevo) {
		this.indicadorClienteNuevo = indicadorClienteNuevo;
	}

	public String getReservado() {
		return reservado;
	}

	public void setReservado(String reservado) {
		this.reservado = reservado;
	}

	@Override
	public String toString() {
		return "OUTConsultaBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", fechaConsulta="
				+ fechaConsulta + ", reservadoUsoFuturo=" + reservadoUsoFuturo + ", claveUsuario=" + claveUsuario
				+ ", nombreUsuario=" + nombreUsuario + ", numeroTelefonoUsuario=" + numeroTelefonoUsuario
				+ ", tipoContratoProducto=" + tipoContratoProducto + ", monedaDelCredito=" + monedaDelCredito
				+ ", importeContrato=" + importeContrato + ", tipoResponsabilidadCuenta=" + tipoResponsabilidadCuenta
				+ ", indicadorClienteNuevo=" + indicadorClienteNuevo + ", reservado=" + reservado + "]";
	}

}
