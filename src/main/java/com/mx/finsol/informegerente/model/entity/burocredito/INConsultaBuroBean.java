package com.mx.finsol.informegerente.model.entity.burocredito;

import java.util.List;

public class INConsultaBuroBean {

	
	private String claveEmpresa;
	private String usuarioConsulta;
	private String diasVigencia;
	private String transaccionId;
	private String origen;

	private List<INPersonaBuroBean> personas;

	public String getClaveEmpresa() {
		return claveEmpresa;
	}

	public void setClaveEmpresa(String claveEmpresa) {
		this.claveEmpresa = claveEmpresa;
	}

	public String getUsuarioConsulta() {
		return usuarioConsulta;
	}

	public void setUsuarioConsulta(String usuarioConsulta) {
		this.usuarioConsulta = usuarioConsulta;
	}

	public String getDiasVigencia() {
		return diasVigencia;
	}

	public void setDiasVigencia(String diasVigencia) {
		this.diasVigencia = diasVigencia;
	}

	public String getTransaccionId() {
		return transaccionId;
	}

	public void setTransaccionId(String transaccionId) {
		this.transaccionId = transaccionId;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public List<INPersonaBuroBean> getPersonas() {
		return personas;
	}

	public void setPersonas(List<INPersonaBuroBean> personas) {
		this.personas = personas;
	}

	@Override
	public String toString() {
		return "INConsultaBuroBean [claveEmpresa=" + claveEmpresa + ", usuarioConsulta=" + usuarioConsulta
				+ ", diasVigencia=" + diasVigencia + ", transaccionId=" + transaccionId + ", origen=" + origen
				+ ", personas=" + personas + "]";
	}

}
