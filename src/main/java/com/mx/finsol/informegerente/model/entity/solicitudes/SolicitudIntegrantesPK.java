package com.mx.finsol.informegerente.model.entity.solicitudes;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SolicitudIntegrantesPK implements Serializable{
	
	private static final long serialVersionUID = 1L;
    private String solicitud;
    private Integer numeroIntegrante;

}
