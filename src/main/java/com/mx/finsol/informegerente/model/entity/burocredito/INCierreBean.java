package com.mx.finsol.informegerente.model.entity.burocredito;

public class INCierreBean {

	
	private String etiquetaEncabezadoSegmento;
	private String longitudRegistro;
	private String marcaFin;

	public String getEtiquetaEncabezadoSegmento() {
		return etiquetaEncabezadoSegmento;
	}

	public void setEtiquetaEncabezadoSegmento(String etiquetaEncabezadoSegmento) {
		this.etiquetaEncabezadoSegmento = etiquetaEncabezadoSegmento;
	}

	public String getLongitudRegistro() {
		return longitudRegistro;
	}

	public void setLongitudRegistro(String longitudRegistro) {
		this.longitudRegistro = longitudRegistro;
	}

	public String getMarcaFin() {
		return marcaFin;
	}

	public void setMarcaFin(String marcaFin) {
		this.marcaFin = marcaFin;
	}

	@Override
	public String toString() {
		return "INCierreBean [etiquetaEncabezadoSegmento=" + etiquetaEncabezadoSegmento + ", longitudRegistro="
				+ longitudRegistro + ", marcaFin=" + marcaFin + "]";
	}
}
