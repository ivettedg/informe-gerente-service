package com.mx.finsol.informegerente.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuroBusquedaDTO {

	
   private String claveEmpresa;
   private String usuarioConsulta;
   private String diasVigencia;
   private String transaccionId;
   private String origen;
   private PersonasDTO personas;
}
